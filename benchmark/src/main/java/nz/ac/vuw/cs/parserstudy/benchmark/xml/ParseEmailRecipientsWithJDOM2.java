package nz.ac.vuw.cs.parserstudy.benchmark.xml;

import nz.ac.vuw.cs.parserstudy.benchmark.ParseEmailRecipients;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import java.io.File;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Parse emails with JDOM2.
 * @author jens dietrich
 */
public class ParseEmailRecipientsWithJDOM2 implements ParseEmailRecipients {

    // main method for testing only
    public static void main(String[] args) throws Exception {
        long t1 = System.currentTimeMillis();
        Set<String> recipients = new ParseEmailRecipientsWithJDOM2().extractRecipient(new File("dataset/emails-XS.xml"));
        long t2 = System.currentTimeMillis();

        System.out.println("Done, found " + recipients.size() + " recipients, this took " + (t2-t1) + " ms");
        System.out.println(recipients);
    }

    @Override
    public Set<String> extractRecipient(File xmlFile) throws Exception {
        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build(xmlFile);
        Element root = doc.getRootElement();
        assert root.getName().equals("emails");
        Set<String> recipients = root.getChildren("email").stream()
            .flatMap(to -> to.getChildren("to").stream())
            .map(to -> to.getChildren("display_name").get(0))
            .map(e -> e.getText())
            .collect(Collectors.toSet());

        return recipients;
    }
}

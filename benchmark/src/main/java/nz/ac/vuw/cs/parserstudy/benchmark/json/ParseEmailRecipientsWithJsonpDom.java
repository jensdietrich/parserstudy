package nz.ac.vuw.cs.parserstudy.benchmark.json;

import nz.ac.vuw.cs.parserstudy.benchmark.ParseEmailRecipients;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.File;
import java.io.FileInputStream;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Parse emails with the jsonp dom parser.
 * @author jens dietrich
 */
public class ParseEmailRecipientsWithJsonpDom implements ParseEmailRecipients {

    // main method for testing only
    public static void main(String[] args) throws Exception {
        long t1 = System.currentTimeMillis();
        Set<String> recipients = new ParseEmailRecipientsWithJsonpDom().extractRecipient(new File("dataset/emails-XS.json"));
        long t2 = System.currentTimeMillis();

        System.out.println("Done, found " + recipients.size() + " recipients, this took " + (t2-t1) + " ms");
        System.out.println(recipients);
    }

    @Override
    public Set<String> extractRecipient(File jsonFile) throws Exception {

        JsonReader rdr = Json.createReader(new FileInputStream(jsonFile));
        JsonObject root = rdr.readObject();
        Set<String> recipients = root.getJsonArray("emails").stream()
            .map(JsonObject.class::cast)
            .flatMap(email -> email.getJsonArray("to").stream())
            .map(JsonObject.class::cast)
            .map(to -> to.getString("displayname"))
            .collect(Collectors.toSet());
        return recipients;
    }
}

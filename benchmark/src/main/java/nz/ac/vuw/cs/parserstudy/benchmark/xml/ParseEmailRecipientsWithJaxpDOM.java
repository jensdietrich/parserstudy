package nz.ac.vuw.cs.parserstudy.benchmark.xml;

import nz.ac.vuw.cs.parserstudy.benchmark.ParseEmailRecipients;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * Parse emails with a JAXP DOM parser.
 * @author jens dietrich
 */
public class ParseEmailRecipientsWithJaxpDOM implements ParseEmailRecipients {

    // main method for testing only
    public static void main(String[] args) throws Exception {
        long t1 = System.currentTimeMillis();
        Set<String> recipients = new ParseEmailRecipientsWithJaxpDOM().extractRecipient(new File("dataset/emails-XS.xml"));
        long t2 = System.currentTimeMillis();

        System.out.println("Done, found " + recipients.size() + " recipients, this took " + (t2-t1) + " ms");
        System.out.println(recipients);
    }

    @Override
    public Set<String> extractRecipient(File xmlFile) throws Exception {
        Set<String> recipients = new HashSet<>();
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(xmlFile);
        Element root = doc.getDocumentElement();
        assert root.getTagName().equals("emails");
        NodeList emailNodes = root.getChildNodes();
        for (int j=0;j<emailNodes.getLength();j++) {
            if (emailNodes.item(j) instanceof Element) {
                Element emailElement = (Element) emailNodes.item(j);
                NodeList toNodes = emailElement.getElementsByTagName("to");
                for (int i = 0; i < toNodes.getLength(); i++) {
                    Element toElement = (Element) toNodes.item(i);
                    Element displayNameElement = (Element) toElement.getElementsByTagName("display_name").item(0);
                    String displayName = displayNameElement.getTextContent();
                    recipients.add(displayName);
                }
            }
        }
        return recipients;
    }
}

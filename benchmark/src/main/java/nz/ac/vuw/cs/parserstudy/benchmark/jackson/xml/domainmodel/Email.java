package nz.ac.vuw.cs.parserstudy.benchmark.jackson.xml.domainmodel;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import java.util.ArrayList;
import java.util.List;

public class Email {
    private String id = null;
    private String subject = null;
    private String body = null;
    private Recipient from = null;

    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Recipient> to = new ArrayList<>();
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Recipient> cc = new ArrayList<>();
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Recipient> bcc = new ArrayList<>();


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Recipient getFrom() {
        return from;
    }

    public void setFrom(Recipient from) {
        this.from = from;
    }

    public List<Recipient> getTo() {
        return to;
    }

    public void setTo(List<Recipient> to) {
        this.to = to;
    }

    public List<Recipient> getCc() {
        return cc;
    }

    public void setCc(List<Recipient> cc) {
        this.cc = cc;
    }

    public List<Recipient> getBcc() {
        return bcc;
    }

    public void setBcc(List<Recipient> bcc) {
        this.bcc = bcc;
    }


}

package nz.ac.vuw.cs.parserstudy.benchmark.json;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import nz.ac.vuw.cs.parserstudy.benchmark.ParseEmailRecipients;
import nz.ac.vuw.cs.parserstudy.benchmark.jackson.json.domainmodel.EmailsHolder;
import nz.ac.vuw.cs.parserstudy.benchmark.jackson.xml.domainmodel.Emails;

import javax.json.JsonObject;
import java.io.File;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Parse emails with jackson databinding.
 * @author jens dietrich
 */
public class ParseEmailRecipientsWithJackson implements ParseEmailRecipients {

    // main method for testing only
    public static void main(String[] args) throws Exception {
        long t1 = System.currentTimeMillis();
        Set<String> recipients = new ParseEmailRecipientsWithJackson().extractRecipient(new File("dataset/emails-XS.json"));
        long t2 = System.currentTimeMillis();

        System.out.println("Done, found " + recipients.size() + " recipients, this took " + (t2-t1) + " ms");
        System.out.println(recipients);
    }

    @Override
    public Set<String> extractRecipient(File jsonFile) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        EmailsHolder emailsHolder = objectMapper.readValue(jsonFile,EmailsHolder.class);
        Set<String> recipients = emailsHolder.getEmails().stream()
            .flatMap(email -> email.getTo().stream())
            .map(recipient -> recipient.getDisplayName())
            .collect(Collectors.toSet());
        return recipients;
    }
}

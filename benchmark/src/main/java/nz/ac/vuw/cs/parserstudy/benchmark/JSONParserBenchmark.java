/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package nz.ac.vuw.cs.parserstudy.benchmark;

import nz.ac.vuw.cs.parserstudy.benchmark.json.*;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;
import java.io.File;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import static nz.ac.vuw.cs.parserstudy.benchmark.BenchmarkSettings.*;


/**
 * Benchmark for JSON parsers.
 * @author jens dietrich
 */
@State(Scope.Benchmark)
public class JSONParserBenchmark {

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
            .include(JSONParserBenchmark.class.getSimpleName())
            .addProfiler(PROFILER)
            .forks(FORKS)
            .warmupIterations(WARMUP_ITERATIONS)
            .measurementIterations(MEASURMENT_ITERATIONS)
            .resultFormat(ResultFormatType.CSV)
            .result("analysis/jmh-result-jsonparserbenchmark.csv")
            .measurementTime(TimeValue.seconds(25))  // increase from default 10
            .build();

        new Runner(opt).run();
    }

    @Param({ "dataset/emails-XS.json","dataset/emails-S.json","dataset/emails-M.json","dataset/emails-L.json","dataset/emails-XL.json","dataset/emails-XXL.json" })
    public String jsonFile;

    // skip to make results easier to read
    //    @Benchmark
    //    @BenchmarkMode(Mode.AverageTime)
    //    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    //    public Set<String> parseWithJsonDotOrg() throws Exception {
    //       return new ParseEmailRecipientsWithJsonDotOrg().extractRecipient(new File(jsonFile));
    //    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public Set<String> parseWithJsonpDom() throws Exception {
        return new ParseEmailRecipientsWithJsonpDom().extractRecipient(new File(jsonFile));
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public Set<String> parseWithjsonpStax() throws Exception {
        return new ParseEmailRecipientsWithJsonpStax().extractRecipient(new File(jsonFile));
    }


    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public Set<String> parseWithJsonPath() throws Exception {
        return new ParseEmailRecipientsWithJsonPath().extractRecipient(new File(jsonFile));
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public Set<String> parseWithJackson() throws Exception {
        return new ParseEmailRecipientsWithJackson().extractRecipient(new File(jsonFile));
    }

}

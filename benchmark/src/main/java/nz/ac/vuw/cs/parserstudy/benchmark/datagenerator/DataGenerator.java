package nz.ac.vuw.cs.parserstudy.benchmark.datagenerator;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Script to generate random data sets of various sizes to be used in benchmarks.
 * @author jens dietrich
 */
public class DataGenerator {

    public static final Map<String,Integer> DATASET_SIZES = new LinkedHashMap<>();

    static {
        DATASET_SIZES.put("XS",5);
        DATASET_SIZES.put("S",50);
        DATASET_SIZES.put("M",500);
        DATASET_SIZES.put("L",5_000);
        DATASET_SIZES.put("XL",50_000);
        DATASET_SIZES.put("XXL",500_000);
        // DATASET_SIZES.put("XXXL",5_000_000);
    }

//    public static final int XS = 3;
//    public static final int S = 10;
//    public static final int M = 1_000;
//    public static final int L = 100_000;
//    public static final int XL = 10_000_000;


    public static void main(String[] args) throws Exception {
        File folder = new File("dataset");
        for (String SN:DATASET_SIZES.keySet()) {
            List<Email> data = generateData(DATASET_SIZES.get(SN));
            File xmlFile = new File(folder,"emails-" + SN + ".xml");
            exportXML(xmlFile,data);
            File jsonFile = new File(folder,"emails-" + SN + ".json");
            exportJSON(jsonFile,data);
        }
    }

    private static void exportXML(File file,List<Email> data) {
        // generate DOM
        Document doc = new Document();
        Element root = new Element("emails");
        doc.setRootElement(root);
        for (Email email:data) {
            Element elEmail = new Element("email");
            Attribute attr = new Attribute("id",""+email.getId());
            elEmail.setAttribute(attr);
            elEmail.addContent(createXMLElement("from",email.getFrom()));
            for (Participant part:email.getTo()) {
                elEmail.addContent(createXMLElement("to",part));
            }
            for (Participant part:email.getCc()) {
                elEmail.addContent(createXMLElement("cc",part));
            }
            for (Participant part:email.getBcc()) {
                elEmail.addContent(createXMLElement("bcc",part));
            }
            elEmail.addContent(createXMLElement("subject",email.getSubject()));
            elEmail.addContent(createXMLElement("body",email.getBody()));
            root.addContent(elEmail);
        }

        // export
        XMLOutputter xmlOutput = new XMLOutputter();

        // display nice nice
        xmlOutput.setFormat(Format.getPrettyFormat());
        try (FileWriter out = new FileWriter(file)) {
            xmlOutput.output(doc,out);
            System.out.println("File written to " + file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void exportJSON(File file,List<Email> data)  {
        JSONObject root = new JSONObject();
        JSONArray emailsObj = new JSONArray();
        for (Email email:data) {
            JSONObject emailObj = new JSONObject();
            emailObj.put("id",email.getId());
            emailObj.put("subject",email.getSubject());
            emailObj.put("subject",email.getSubject());
            emailObj.put("from",createJSONObject(email.getFrom()));
            emailObj.put("to",createJSONArray(email.getTo()));
            emailObj.put("cc",createJSONArray(email.getCc()));
            emailObj.put("bcc",createJSONArray(email.getBcc()));
            emailsObj.put(emailObj);
        }
        root.put("emails",emailsObj);

        try (FileWriter out = new FileWriter(file)) {
            root.write(out,1,1);
            System.out.println("File written to " + file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private static JSONArray createJSONArray(List<Participant> participants) {
        JSONArray array = new JSONArray();
        for (Participant participant:participants) {
            array.put(createJSONObject(participant));
        }
        return array;
    }

    private static JSONObject createJSONObject(Participant participant) {
        JSONObject obj = new JSONObject();
        obj.put("displayname",participant.getDisplayName());
        obj.put("emailaddress",participant.getEmailAddress());
        return obj;
    }


    private static Element createXMLElement(String name, Participant data) {
        Element el = new Element(name);
        Element elDisplayName = new Element("display_name");
        elDisplayName.addContent(data.getDisplayName());
        el.addContent(elDisplayName);
        Element elEmail = new Element("email_address");
        elEmail.addContent(data.getEmailAddress());
        el.addContent(elEmail);
        return el;
    }

    private static Element createXMLElement(String name, String data) {
        Element el = new Element(name);
        el.addContent(data);
        return el;
    }

    private static List<Email> generateData(int size) {
        List<Email> list = new ArrayList<>();
        for (int i=0;i<size;i++) {
            list.add(Email.randomInstance());
        }
        return list;
    }
}

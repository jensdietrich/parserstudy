package nz.ac.vuw.cs.parserstudy.benchmark.xml;

import nz.ac.vuw.cs.parserstudy.benchmark.ParseEmailRecipients;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * Parse emails with a JAXP SAX parser.
 * @author jens dietrich
 */
public class ParseEmailRecipientsWithJaxpSAX implements ParseEmailRecipients {

    // main method for testing only
    public static void main(String[] args) throws Exception {
        long t1 = System.currentTimeMillis();
        Set<String> recipients = new ParseEmailRecipientsWithJaxpSAX().extractRecipient(new File("dataset/emails-XS.xml"));
        long t2 = System.currentTimeMillis();

        System.out.println("Done, found " + recipients.size() + " recipients, this took " + (t2-t1) + " ms");
        System.out.println(recipients);
    }

    @Override
    public Set<String> extractRecipient(File xmlFile) throws Exception {
        Set<String> recipients = new HashSet<>();
        InputSource input = new InputSource(xmlFile.getAbsolutePath());

        // context is modelled as a stack, to make sure only the display_names within to (and not from,cc, and bcc)
        // are recorded
        DefaultHandler handler = new DefaultHandler() {
            private Stack<String> context = new Stack<>();
            @Override
            public void startElement(String uri, String localName, String qName, Attributes attributes) {
                context.push(qName);
            }

            @Override
            public void endElement(String uri, String localName, String qName) {
                context.pop();
            }

            @Override
            public void characters(char[] ch, int start, int length) {
                if (context.size()>=2 && context.peek().equals("display_name") && context.get(context.size()-2).equals("to")) {
                    char[] data = new char[length];
                    for (int i=start;i<(start+length);i++) data[i-start]=ch[i];
                    recipients.add(new String(data));
                }
            }
        };

        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        saxParser.parse(input, handler);
        return recipients;
    }
}

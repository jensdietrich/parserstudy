package nz.ac.vuw.cs.parserstudy.benchmark.datagenerator;

/**
 * Domain class used to generate test data.
 * @author jens dietrich
 */
public class Participant {

    static Participant randomInstance() {
        Participant participant = new Participant();
        String fname = RandomUtils.getRandomAlphabeticString(2,10);
        String lname = RandomUtils.getRandomAlphabeticString(2,10);
        participant.displayName = fname + " " + lname;
        String domain = RandomUtils.getRandomAlphabeticString(2,6);
        String topLevelDomain = RandomUtils.pickRandom("org","net","com","edu","de","ch","fr","ru","jp","cn","ac.uk","ac.nz","edu.au","co.uk") ;
        participant.emailAddress = fname + '.' + lname + '@' + domain + '.' + topLevelDomain;
        return participant;
    }


    private String emailAddress = null;
    private String displayName = null;

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Participant that = (Participant) o;

        if (!emailAddress.equals(that.emailAddress)) return false;
        return displayName.equals(that.displayName);
    }

    @Override
    public int hashCode() {
        int result = emailAddress.hashCode();
        result = 31 * result + displayName.hashCode();
        return result;
    }
}

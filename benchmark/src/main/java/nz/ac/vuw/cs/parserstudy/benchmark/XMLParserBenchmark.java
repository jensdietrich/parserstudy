/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package nz.ac.vuw.cs.parserstudy.benchmark;

import nz.ac.vuw.cs.parserstudy.benchmark.xml.*;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.io.File;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import static nz.ac.vuw.cs.parserstudy.benchmark.BenchmarkSettings.*;

/**
 * Benchmark for XML parsers.
 * @author jens dietrich
 */
@State(Scope.Benchmark)
public class XMLParserBenchmark {

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
            .include(XMLParserBenchmark.class.getSimpleName())
            .addProfiler(PROFILER)
            .forks(FORKS)
            .warmupIterations(WARMUP_ITERATIONS)  // for testing only
            .measurementIterations(MEASURMENT_ITERATIONS) // for testing only
            .resultFormat(ResultFormatType.CSV)
            .result("analysis/jmh-result-xmlparserbenchmark.csv")
            .measurementTime(MEASURMENT_TIME)  // increase from default 10
            .build();

        new Runner(opt).run();
    }

    @Param({ "dataset/emails-XS.xml","dataset/emails-S.xml","dataset/emails-M.xml","dataset/emails-L.xml","dataset/emails-XL.xml","dataset/emails-XXL.xml" })
    public String xmlFile;

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public Set<String> parseWithJaxpDOM() throws Exception {
       return new ParseEmailRecipientsWithJaxpDOM().extractRecipient(new File(xmlFile));
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public Set<String> parseWithJaxpSAX() throws Exception {
        return new ParseEmailRecipientsWithJaxpSAX().extractRecipient(new File(xmlFile));
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public Set<String> parseWithJaxpSTAX() throws Exception {
        return new ParseEmailRecipientsWithJaxpSTAX().extractRecipient(new File(xmlFile));
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public Set<String> parseWithJDOM2() throws Exception {
        return new ParseEmailRecipientsWithJDOM2().extractRecipient(new File(xmlFile));
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public Set<String> parseWithJaxpXPath() throws Exception {
        return new ParseEmailRecipientsWithJaxpXPATH().extractRecipient(new File(xmlFile));
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public Set<String> parseWithJAXB() throws Exception {
        return new ParseEmailRecipientsWithJAXB().extractRecipient(new File(xmlFile));
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public Set<String> parseWithJackson() throws Exception {
        return new ParseEmailRecipientsWithJackson().extractRecipient(new File(xmlFile));
    }

}

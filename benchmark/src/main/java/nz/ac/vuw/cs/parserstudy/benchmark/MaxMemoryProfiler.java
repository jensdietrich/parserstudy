package nz.ac.vuw.cs.parserstudy.benchmark;

import org.openjdk.jmh.infra.BenchmarkParams;
import org.openjdk.jmh.infra.IterationParams;
import org.openjdk.jmh.profile.InternalProfiler;
import org.openjdk.jmh.results.AggregationPolicy;
import org.openjdk.jmh.results.IterationResult;
import org.openjdk.jmh.results.Result;
import org.openjdk.jmh.results.ScalarResult;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Very coarse measurment of max memory at the end of an iteration.
 * @author jens dietrich
 */

public class MaxMemoryProfiler implements InternalProfiler {



    @Override
    public String getDescription() {
        return "Max memory heap profiler";
    }

    @Override
    public void beforeIteration(BenchmarkParams benchmarkParams, IterationParams iterationParams) {

    }

    @Override
    public Collection<? extends Result> afterIteration(BenchmarkParams benchmarkParams, IterationParams iterationParams, IterationResult result) {

        System.out.println("max memory analysis " + Thread.currentThread().getName());

        long totalHeap = Runtime.getRuntime().totalMemory() / 1_000_000;
        Collection<ScalarResult> results = new ArrayList<>();
        results.add(new ScalarResult("max heap", totalHeap, "MB", AggregationPolicy.MAX));

//        totalHeap = getReallyUsedMemory();
//        results.add(new ScalarResult("max heap after GC", totalHeap, "bytes", AggregationPolicy.MAX));

        return results;
    }



    // alternative measure as discussed in https://cruftex.net/2017/03/28/The-6-Memory-Metrics-You-Should-Track-in-Your-Java-Benchmarks.html#metric-used-memory-after-forced-gc
    // note that afterIteration is invoked in another thread, and using this there is a chance that the objects
    // returned by the benchmarks would be garbage collected (TODO: how does JMH manage references to returned objects ?)

//    private long getCurrentlyUsedMemory() {
//        return
//                ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed();//+ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage().getUsed();
//    }
//    private long getGcCount() {
//        long sum = 0;
//        for (GarbageCollectorMXBean b : ManagementFactory.getGarbageCollectorMXBeans()) {
//            long count = b.getCollectionCount();
//            if (count != -1) { sum +=  count; }
//        }
//        return sum;
//    }
//    private long getReallyUsedMemory() {
//        long before = getGcCount();
//        System.gc();
//        while (getGcCount() == before);
//        return getCurrentlyUsedMemory();
//    }

}
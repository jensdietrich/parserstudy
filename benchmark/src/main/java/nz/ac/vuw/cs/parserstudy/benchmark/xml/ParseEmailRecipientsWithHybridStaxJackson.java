package nz.ac.vuw.cs.parserstudy.benchmark.xml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import nz.ac.vuw.cs.parserstudy.benchmark.ParseEmailRecipients;
import nz.ac.vuw.cs.parserstudy.benchmark.jackson.xml.domainmodel.Recipient;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashSet;
import java.util.Set;

/**
 * Parse emails with jackson xml databinding.
 * This is a hybrid solution where not the entire tree, but only relevant parts are desrialized into
 * objects, otherwise the document is stax-parsed.
 * @author jens dietrich
 */
public class ParseEmailRecipientsWithHybridStaxJackson implements ParseEmailRecipients {

    // main method for testing only
    public static void main(String[] args) throws Exception {
        long t1 = System.currentTimeMillis();
        Set<String> recipients = new ParseEmailRecipientsWithHybridStaxJackson().extractRecipient(new File("dataset/emails-XS.xml"));
        long t2 = System.currentTimeMillis();

        System.out.println("Done, found " + recipients.size() + " recipients, this took " + (t2-t1) + " ms");
        System.out.println(recipients);
    }

    @Override
    public Set<String> extractRecipient(File xmlFile) throws Exception {

        Set<String> recipients = new HashSet<>();
        XMLInputFactory f = XMLInputFactory.newFactory();
        XMLStreamReader sr = f.createXMLStreamReader(new FileInputStream(xmlFile));
        XmlMapper objectMapper = new XmlMapper();
        while (sr.hasNext()) {
            sr.next();
            if (sr.isStartElement() && "to".equals(sr.getLocalName())) {
                Recipient participant = objectMapper.readValue(sr, Recipient.class);
                recipients.add(participant.getDisplayName());
            }
        }

        return recipients;
    }
}

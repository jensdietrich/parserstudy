package nz.ac.vuw.cs.parserstudy.benchmark.xml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import nz.ac.vuw.cs.parserstudy.benchmark.ParseEmailRecipients;
import nz.ac.vuw.cs.parserstudy.benchmark.jackson.xml.domainmodel.*;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Parse emails with jackson xml databinding.
 * @author jens dietrich
 */
public class ParseEmailRecipientsWithJackson implements ParseEmailRecipients {

    // main method for testing only
    public static void main(String[] args) throws Exception {
        long t1 = System.currentTimeMillis();
        Set<String> recipients = new ParseEmailRecipientsWithJackson().extractRecipient(new File("dataset/emails-XS.xml"));
        long t2 = System.currentTimeMillis();

        System.out.println("Done, found " + recipients.size() + " recipients, this took " + (t2-t1) + " ms");
        System.out.println(recipients);
    }

    @Override
    public Set<String> extractRecipient(File xmlFile) throws Exception {

        XMLInputFactory f = XMLInputFactory.newFactory();
        XMLStreamReader sr = f.createXMLStreamReader(new FileInputStream(xmlFile));
        XmlMapper objectMapper = new XmlMapper();
        Emails emails = objectMapper.readValue(sr,Emails.class);
        Set<String> recipients = emails.stream()
            .flatMap(email -> email.getTo().stream())
            .map(recipient -> recipient.getDisplayName())
            .collect(Collectors.toSet());
        return recipients;
    }
}

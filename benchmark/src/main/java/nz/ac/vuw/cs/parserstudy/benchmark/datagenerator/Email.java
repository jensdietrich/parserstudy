package nz.ac.vuw.cs.parserstudy.benchmark.datagenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Domain class used to generate test data.
 * @author jens dietrich
 */
public class Email {

    static AtomicInteger counter = new AtomicInteger(0);
    static Email randomInstance() {
        Email email = new Email();
        email.id = counter.incrementAndGet();
        email.setSubject(RandomUtils.getRandomAlphabeticString(3,10,1,3));
        email.setBody(RandomUtils.getRandomAlphabeticString(3,20,1,3));
        email.setFrom(Participant.randomInstance());
        for (int i=0;i<RandomUtils.getRandomPositiveNumberFromExponentialDistribution(3,7);i++) {
            email.to.add(Participant.randomInstance());
        }
        for (int i=0;i<RandomUtils.getRandomPositiveNumberFromExponentialDistribution(3,7);i++) {
            email.cc.add(Participant.randomInstance());
        }
        for (int i=0;i<RandomUtils.getRandomPositiveNumberFromExponentialDistribution(3,7);i++) {
            email.bcc.add(Participant.randomInstance());
        }
        return email;
    }

    private int id = 0;
    private String subject = null;
    private String body = null;
    private List<Participant> to = new ArrayList<>();
    private List<Participant> cc = new ArrayList<>();
    private List<Participant> bcc = new ArrayList<>();
    private Participant from = null;

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setFrom(Participant from) {
        this.from = from;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public List<Participant> getTo() {
        return to;
    }

    public List<Participant> getCc() {
        return cc;
    }

    public List<Participant> getBcc() {
        return bcc;
    }

    public Participant getFrom() {
        return from;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Email email = (Email) o;

        if (id != email.id) return false;
        if (subject != null ? !subject.equals(email.subject) : email.subject != null) return false;
        if (body != null ? !body.equals(email.body) : email.body != null) return false;
        if (to != null ? !to.equals(email.to) : email.to != null) return false;
        if (cc != null ? !cc.equals(email.cc) : email.cc != null) return false;
        if (bcc != null ? !bcc.equals(email.bcc) : email.bcc != null) return false;
        return from != null ? from.equals(email.from) : email.from == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + (body != null ? body.hashCode() : 0);
        result = 31 * result + (to != null ? to.hashCode() : 0);
        result = 31 * result + (cc != null ? cc.hashCode() : 0);
        result = 31 * result + (bcc != null ? bcc.hashCode() : 0);
        result = 31 * result + (from != null ? from.hashCode() : 0);
        return result;
    }
}

package nz.ac.vuw.cs.parserstudy.benchmark.jackson.xml.domainmodel;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.ArrayList;

@JacksonXmlRootElement(localName = "emails")
public class Emails extends ArrayList<Email> {
}

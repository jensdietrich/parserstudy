package nz.ac.vuw.cs.parserstudy.benchmark.xml;

import nz.ac.vuw.cs.parserstudy.benchmark.ParseEmailRecipients;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

/**
 * Parse emails with a JAXP STAX parser.
 * @author jens dietrich
 */
public class ParseEmailRecipientsWithJaxpSTAX implements ParseEmailRecipients {

    // main method for testing only
    public static void main(String[] args) throws Exception {
        long t1 = System.currentTimeMillis();
        Set<String> recipients = new ParseEmailRecipientsWithJaxpSTAX().extractRecipient(new File("dataset/emails-XS.xml"));
        long t2 = System.currentTimeMillis();

        System.out.println("Done, found " + recipients.size() + " recipients, this took " + (t2-t1) + " ms");
        System.out.println(recipients);
    }

    @Override
    public Set<String> extractRecipient(File xmlFile) throws Exception {
        Set<String> recipients = new HashSet<>();
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        InputStream in = new FileInputStream(xmlFile);
        XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

        // context is modelled as a stack, to make sure only the display_names within to (and not from,cc, and bcc)
        // are recorded
        Stack<String> context = new Stack<>();

        while(eventReader.hasNext()){
            XMLEvent event = eventReader.nextEvent();
            if (event.isStartElement()) {
                context.push(event.asStartElement().getName().getLocalPart());
            }
            else if (event.isEndElement()) {
                context.pop();
            }
            else if (event.isCharacters() && context.size()>=2 && context.peek().equals("display_name") && context.get(context.size()-2).equals("to")) {
                recipients.add(event.asCharacters().getData());
            }
        }
        return recipients;
    }
}

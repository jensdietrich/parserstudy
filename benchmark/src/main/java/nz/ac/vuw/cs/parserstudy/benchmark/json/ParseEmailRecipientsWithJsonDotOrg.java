package nz.ac.vuw.cs.parserstudy.benchmark.json;

import nz.ac.vuw.cs.parserstudy.benchmark.ParseEmailRecipients;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Parse emails with the json.org dom parser.
 * @author jens dietrich
 */
public class ParseEmailRecipientsWithJsonDotOrg implements ParseEmailRecipients {

    // main method for testing only
    public static void main(String[] args) throws Exception {
        long t1 = System.currentTimeMillis();
        Set<String> recipients = new ParseEmailRecipientsWithJsonDotOrg().extractRecipient(new File("dataset/emails-XS.json"));
        long t2 = System.currentTimeMillis();

        System.out.println("Done, found " + recipients.size() + " recipients, this took " + (t2-t1) + " ms");
        System.out.println(recipients);
    }

    @Override
    public Set<String> extractRecipient(File jsonFile) throws Exception {

        Set<String> recipients = new HashSet<>();

        JSONTokener tokener = new JSONTokener(new FileInputStream(jsonFile));
        JSONObject root = new JSONObject(tokener);
        JSONArray emailsArr = root.getJSONArray("emails");
        for (int i=0;i<emailsArr.length();i++) {
            JSONObject emailObj = emailsArr.getJSONObject(i);
            JSONArray toArr = emailObj.getJSONArray("to");
            for (int j=0;j<toArr.length();j++) {
                String recipient = toArr.getJSONObject(j).getString("displayname");
                recipients.add(recipient);
            }
        }

        return recipients;
    }
}

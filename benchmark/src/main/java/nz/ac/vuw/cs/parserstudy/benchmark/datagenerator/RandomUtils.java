package nz.ac.vuw.cs.parserstudy.benchmark.datagenerator;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.apache.commons.math3.distribution.RealDistribution;

import java.util.Random;

/**
 * Random utilities.
 * @author jens dietrich
 */
public class RandomUtils {

    private static final Random RANDOM = new Random();

    public static String getRandomAlphabeticString(int minLength,int maxLength) {
        if (maxLength<=minLength) throw new IllegalArgumentException();
        return RandomStringUtils.randomAlphabetic(minLength+RANDOM.nextInt(maxLength-minLength));
    }

    public static String getRandomAlphabeticString(int minLength,int maxLength,int minTokens,int maxTokens) {
        if (maxLength<=minLength) throw new IllegalArgumentException();
        if (maxTokens<=minTokens) throw new IllegalArgumentException();
        int count = minTokens + RANDOM.nextInt(maxLength-minLength);
        String s = null;
        for (int i=0;i<count;i++) {
            s = s==null?"":s+" ";
            s = s + getRandomAlphabeticString(minLength,maxLength);
        }
        return s;
    }

    public static String pickRandom(String... options) {
        return options[RANDOM.nextInt(options.length)];
    }

    public static int getRandomPositiveNumberFromExponentialDistribution(double mean,int max) {
        RealDistribution distribution = new ExponentialDistribution(mean);
        double dvalue = distribution.sample();
        int value = (int)Math.round(dvalue);
        if (value>max) {
            // try again -- careful with recursion at low max values !
            return getRandomPositiveNumberFromExponentialDistribution(mean,max);
        }
        else {
            return value;
        }
    }

}

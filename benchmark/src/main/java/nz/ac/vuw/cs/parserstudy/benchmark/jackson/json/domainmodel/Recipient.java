package nz.ac.vuw.cs.parserstudy.benchmark.jackson.json.domainmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Recipient {

    @JsonProperty("displayname")
    private String displayName = null;

    @JsonProperty("emailaddress")
    private String emailAddress = null;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Recipient recipient = (Recipient) o;

        if (!displayName.equals(recipient.displayName)) return false;
        return emailAddress.equals(recipient.emailAddress);
    }

    @Override
    public int hashCode() {
        int result = displayName.hashCode();
        result = 31 * result + emailAddress.hashCode();
        return result;
    }
}

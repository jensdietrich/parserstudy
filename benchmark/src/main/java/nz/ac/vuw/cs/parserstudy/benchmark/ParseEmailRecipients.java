package nz.ac.vuw.cs.parserstudy.benchmark;

import java.io.File;
import java.util.Set;

/**
 * Interface of a service that extracts the list of display names of the email recipients (to only, exclud. cc and bcc) from
 * a list of emails.
 * @author jens dietrich
 */
public interface ParseEmailRecipients {

    Set<String> extractRecipient(File data) throws Exception;
}

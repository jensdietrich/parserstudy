package nz.ac.vuw.cs.parserstudy.benchmark;

import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.options.TimeValue;

/**
 * Centralised settings used by JHM.
 * @author jens dietrich
 */
public class BenchmarkSettings {
    public static final String PROFILER = "gc";
    public static final int FORKS = 1;
    public static final int WARMUP_ITERATIONS = 5;
    public static final int MEASURMENT_ITERATIONS = 10;
    public static final ResultFormatType RESULT_FORMAT_TYPE = ResultFormatType.CSV;
    public static final TimeValue MEASURMENT_TIME = TimeValue.seconds(25);
}

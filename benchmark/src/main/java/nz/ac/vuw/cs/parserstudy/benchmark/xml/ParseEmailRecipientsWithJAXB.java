package nz.ac.vuw.cs.parserstudy.benchmark.xml;

import nz.ac.vuw.cs.parserstudy.benchmark.ParseEmailRecipients;
import nz.ac.vuw.cs.parserstudy.benchmark.jaxb.generated.*;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * Parse emails with the JAXB-generated parser.
 * @author jens dietrich
 */
public class ParseEmailRecipientsWithJAXB implements ParseEmailRecipients {

    // main method for testing only
    public static void main(String[] args) throws Exception {
        long t1 = System.currentTimeMillis();
        Set<String> recipients = new ParseEmailRecipientsWithJAXB().extractRecipient(new File("dataset/emails-XXL.xml"));
        long t2 = System.currentTimeMillis();
        System.out.println("Done, found " + recipients.size() + " recipients, this took " + (t2-t1) + " ms");
        System.out.println(recipients);
    }

    @Override
    public Set<String> extractRecipient(File xmlFile) throws Exception {
        Set<String> recipients = new HashSet<>();
        JAXBContext jc = JAXBContext.newInstance( "nz.ac.vuw.cs.parserstudy.benchmark.jaxb.generated" );
        Unmarshaller parser = jc.createUnmarshaller();
        Emails root = (Emails) parser.unmarshal(xmlFile);
        for (Email email:root.getEmail()) {
            for (Participant participant:email.getTo()) {
                recipients.add(participant.getDisplayName());
            }
        }
        return recipients;
    }
}

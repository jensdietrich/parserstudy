package nz.ac.vuw.cs.parserstudy.benchmark.jackson.json.domainmodel;

// type for the document root
public class EmailsHolder {
    private Emails emails = null;

    public Emails getEmails() {
        return emails;
    }

    public void setEmails(Emails emails) {
        this.emails = emails;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmailsHolder that = (EmailsHolder) o;

        return emails != null ? emails.equals(that.emails) : that.emails == null;
    }

    @Override
    public int hashCode() {
        return emails != null ? emails.hashCode() : 0;
    }
}

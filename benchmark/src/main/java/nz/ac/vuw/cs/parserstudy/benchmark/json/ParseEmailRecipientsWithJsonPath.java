package nz.ac.vuw.cs.parserstudy.benchmark.json;

import com.jayway.jsonpath.JsonPath;
import net.minidev.json.JSONArray;
import nz.ac.vuw.cs.parserstudy.benchmark.ParseEmailRecipients;
import java.io.File;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Parse emails with jayway JsonPath.
 * @author jens dietrich
 */
public class ParseEmailRecipientsWithJsonPath implements ParseEmailRecipients {

    // main method for testing only
    public static void main(String[] args) throws Exception {
        long t1 = System.currentTimeMillis();
        Set<String> recipients = new ParseEmailRecipientsWithJsonPath().extractRecipient(new File("dataset/emails-XS.json"));
        long t2 = System.currentTimeMillis();

        System.out.println("Done, found " + recipients.size() + " recipients, this took " + (t2-t1) + " ms");
        System.out.println(recipients);
    }

    @Override
    public Set<String> extractRecipient(File jsonFile) throws Exception {

        JSONArray results = JsonPath.parse(jsonFile)
                .read("$.emails[*].to[*].displayname", JSONArray.class);
        Set<String> recipients = results.stream()
                .map(String.class::cast)
                .collect(Collectors.toSet());

        return recipients;
    }
}

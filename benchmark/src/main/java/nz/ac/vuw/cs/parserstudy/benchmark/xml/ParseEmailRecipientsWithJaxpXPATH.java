package nz.ac.vuw.cs.parserstudy.benchmark.xml;

import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.util.HashSet;
import java.util.Set;
import nz.ac.vuw.cs.parserstudy.benchmark.ParseEmailRecipients;

/**
 * Parse emails with a JAXP XPATH.
 * @author jens dietrich
 */
public class ParseEmailRecipientsWithJaxpXPATH implements ParseEmailRecipients {

    // main method for testing only
    public static void main(String[] args) throws Exception {
        long t1 = System.currentTimeMillis();
        Set<String> recipients = new ParseEmailRecipientsWithJaxpXPATH().extractRecipient(new File("dataset/emails-XS.xml"));
        long t2 = System.currentTimeMillis();

        System.out.println("Done, found " + recipients.size() + " recipients, this took " + (t2-t1) + " ms");
        System.out.println(recipients);
    }

    @Override
    public Set<String> extractRecipient(File xmlFile) throws Exception {
        Set<String> recipients = new HashSet<>();
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        XPathExpression expression = xpath.compile("/emails/email/to/display_name");
        NodeList result = (NodeList)expression.evaluate(new InputSource(xmlFile.getAbsolutePath()), XPathConstants.NODESET);
        for (int i=0;i<result.getLength();i++) {
            recipients.add(result.item(i).getTextContent());
        }
        return recipients;
    }
}

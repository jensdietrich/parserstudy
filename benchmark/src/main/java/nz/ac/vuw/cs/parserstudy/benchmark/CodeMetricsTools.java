package nz.ac.vuw.cs.parserstudy.benchmark;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.expr.CastExpr;
import com.github.javaparser.ast.expr.LambdaExpr;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.ObjectCreationExpr;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.visitor.VoidVisitor;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.google.common.base.Preconditions;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Extract code metrics, and report them in a latex file.
 * @author jens dietrich
 */
public class CodeMetricsTools {

    enum METRIC {
        CASTS , LOOPS, LOOP_DEPTH, TYPE_DEFS, METH_DEFS, ALLOCS, INVOKS, TYPES
    }


    public static void main (String[] args) throws Exception {
        Map<String, Map<METRIC,Object>> metrics = new LinkedHashMap<>();
        Files.walk(Paths.get("src/main/java"))
            .filter(Files::isRegularFile)
            .filter(f -> f.getFileName().toString().endsWith(".java"))
            .filter(f -> f.getFileName().toString().startsWith("ParseEmailRecipients"))
            .filter(f -> !f.getFileName().toString().equals("ParseEmailRecipients.java")) // this is the interface
            // enable next line for debugging
            // .filter(f -> f.getFileName().toString().startsWith("ParseEmailRecipientsWithJDOM2"))
            .forEach(f -> computeMetrics(f.toFile(),metrics));


        // print summary to console
        System.out.println();
        for (String src:metrics.keySet()) {
            Map<METRIC,Object> values = metrics.get(src);
            for (METRIC key:values.keySet()) {
                System.out.println(src + "\t" + key + "\t" + "\t->\t" + values.get(key));
            }
            System.out.println();
        }

        // output latex
        // TODO change output to file
        try (PrintStream out = System.out) {
            out.println("\\begin{table*}[]");
            out.println("\t\\begin{tabular}{|llllllll|}");
            out.println("\t\t\\hline");
            out.println("\t\tparser\t&\t" + Stream.of(METRIC.values()).map(m -> m.name().replace("_","\\_")).collect(Collectors.joining("\t&\t")) +  "\t\\\\ \\hline");
            for (String src:metrics.keySet()) {
                Map<METRIC,Object> values = metrics.get(src);
                out.println("\t\t" + getParserName(src) + "\t&\t"+ Stream.of(METRIC.values()).map(m -> values.get(m)).map(v -> v==null?"":v.toString()).collect(Collectors.joining("\t&\t")) + "\t\\\\");
            }
            out.println("\t\t\\hline");
            out.println("\t\\end{tabular}");
            out.println("\\end{table*}");
            out.println();
        }
    }

    private static String getParserName(String src) {
        Preconditions.checkArgument(src.startsWith("ParseEmailRecipientsWith"));
        Preconditions.checkArgument(src.endsWith(".java"));
        String name = src.substring(0,src.length()-5); // chop off .java
        name = name.substring(24);

        if (name.startsWith("Jaxp"))  {
            name = "JAXP-" + name.substring(4).toUpperCase();
        }
        else if (name.startsWith("Jsonp")) {
            name = "JSONP-" + name.substring(5).toUpperCase();
        }
        return name;
    }

    private static void computeMetrics(File src,Map<String, Map<METRIC,Object>> metrics)  {
        CompilationUnit cu = null;
        try {
            cu = StaticJavaParser.parse(src);
            TypeDeclaration classDecl = cu.getPrimaryType().get();

            for (MethodDeclaration methodDecl:classDecl.findAll(MethodDeclaration.class)) {
                if (methodDecl.getNameAsString().equals("extractRecipient")) {
                    // for debugging only
//                    YamlPrinter printer = new YamlPrinter(true);
//                    System.out.println(printer.output(methodDecl));

                    computeMetrics(src.getName(),methodDecl,metrics);
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void computeMetrics(String name,MethodDeclaration methodDecl,Map<String, Map<METRIC,Object>> metrics) {

        metrics.put(name,new HashMap<>());

        // prepopulate with 0 for all keys
        Stream.of(METRIC.values()).forEach(
            m -> metrics.get(name).put(m,0)
        );

        Set<String> usedTypes = new HashSet<>();
        VoidVisitor visitor = new VoidVisitorAdapter() {
            @Override
            public void visit(CastExpr n, Object arg) {
                super.visit(n,arg);
                // System.out.println("Cast found: " + n);
                metrics.get(name).compute(METRIC.CASTS,(k,v) -> v==null?1:((int)v)+1);
            }

            @Override
            public void visit(ForStmt n, Object arg) {
                super.visit(n, arg);
                // System.out.println("For stmnt found: " + n);
                metrics.get(name).compute(METRIC.LOOPS,(k,v) -> v==null?1:((int)v)+1);
                updateLoopDepth(n);
            }

            @Override
            public void visit(ForEachStmt n, Object arg) {
                super.visit(n, arg);
                // System.out.println("For Each stmnt found: " + n);
                metrics.get(name).compute(METRIC.LOOPS,(k,v) -> v==null?1:((int)v)+1);
                updateLoopDepth(n);
            }

            @Override
            public void visit(WhileStmt n, Object arg) {
                super.visit(n, arg);
                metrics.get(name).compute(METRIC.LOOPS,(k,v) -> v==null?1:((int)v)+1);
                updateLoopDepth(n);
            }

            @Override
            public void visit(ClassOrInterfaceDeclaration n, Object arg) {
                super.visit(n, arg);
                System.out.println("ClassOrInterfaceDeclaration found: " + n);
                metrics.get(name).compute(METRIC.TYPE_DEFS,(k,v) -> v==null?1:((int)v)+1);
            }

            @Override
            public void visit(LocalClassDeclarationStmt n, Object arg) {
                super.visit(n, arg);
                System.out.println("LocalClassDeclarationStmt found: " + n);
                metrics.get(name).compute(METRIC.TYPE_DEFS,(k,v) -> v==null?1:((int)v)+1);
            }

//            @Override
//            public void visit(ClassExpr n, Object arg) {
//                super.visit(n, arg);
//                System.out.println("Class expr found: " + n);
//            }

            @Override
            public void visit(ExplicitConstructorInvocationStmt n, Object arg) {
                super.visit(n, arg);
                System.out.println("Construct inv found: " + n);
            }

//            @Override
//            public void visit(CompilationUnit n, Object arg) {
//                super.visit(n, arg);
//                System.out.println("CompilationUnit found: " + n);
//            }
//
//            @Override
//            public void visit(BlockStmt n, Object arg) {
//                super.visit(n, arg);
//                System.out.println("BlockStmt found: " + n);
//            }

            // could count this -- but need set to control duplicates
            @Override
            public void visit(ClassOrInterfaceType n, Object arg) {
                super.visit(n, arg);
                if (usedTypes.add(n.getName().toString())) {
                    metrics.get(name).compute(METRIC.TYPES,(k,v) -> v==null?1:((int)v)+1);
                }
                // System.out.println("Class or interface type found: " + n);
            }

            @Override
            public void visit(LambdaExpr n, Object arg) {
                super.visit(n, arg);
                System.out.println("Lambda expr found: " + n);
            }

            @Override
            public void visit(MethodDeclaration n, Object arg) {
                super.visit(n, arg);
                // System.out.println("MethodDeclaration found: " + n);
                metrics.get(name).compute(METRIC.METH_DEFS,(k, v) -> v==null?1:((int)v)+1);
            }

            @Override
            public void visit(ObjectCreationExpr n, Object arg) {
                super.visit(n, arg);
                metrics.get(name).compute(METRIC.ALLOCS,(k, v) -> v==null?1:((int)v)+1);
                if (n.getAnonymousClassBody().isPresent()) {
                    metrics.get(name).compute(METRIC.TYPE_DEFS,(k,v) -> v==null?1:((int)v)+1);
                }
            }

            @Override
            public void visit(MethodCallExpr n, Object arg) {
                super.visit(n, arg);
                // System.out.println("Method invocation found: " + n);
                metrics.get(name).compute(METRIC.INVOKS,(k, v) -> v==null?1:((int)v)+1);
                if (n.getNameAsString().equals("stream") || n.getNameAsString().equals("parallelStream")) { // note: assume that flatMap also corresponds to a stream
                    metrics.get(name).compute(METRIC.LOOPS,(k, v) -> v==null?1:((int)v)+1);
                }
            }


            private void updateLoopDepth(Statement n) {
                int counter = 0;
                Node s = n;

                while (s != methodDecl) {
                    if (isLoopStatement(s)) {
                        counter = counter +1;
                    }
                    s = s.getParentNode().get();
                }
                int counter2 = counter;

                metrics.get(name).compute(METRIC.LOOP_DEPTH,(k, v) -> v==null?counter2:Math.max((int)v,counter2));

            }

            private boolean isLoopStatement(Node n) {
                return n instanceof ForStmt || n instanceof ForEachStmt || n instanceof WhileStmt ;
            }
        };

        methodDecl.accept(visitor,null);

    }


}

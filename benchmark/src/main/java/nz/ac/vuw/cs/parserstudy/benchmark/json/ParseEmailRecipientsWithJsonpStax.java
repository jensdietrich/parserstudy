package nz.ac.vuw.cs.parserstudy.benchmark.json;

import nz.ac.vuw.cs.parserstudy.benchmark.ParseEmailRecipients;

import javax.json.Json;
import javax.json.stream.JsonParser;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashSet;
import java.util.Set;

/**
 * Parse emails with the jsonp stax parser.
 * @author jens dietrich
 */
public class ParseEmailRecipientsWithJsonpStax implements ParseEmailRecipients {

    // main method for testing only
    public static void main(String[] args) throws Exception {
        long t1 = System.currentTimeMillis();
        Set<String> recipients = new ParseEmailRecipientsWithJsonpStax().extractRecipient(new File("dataset/emails-XS.json"));
        long t2 = System.currentTimeMillis();

        System.out.println("Done, found " + recipients.size() + " recipients, this took " + (t2-t1) + " ms");
        System.out.println(recipients);
    }

    @Override
    public Set<String> extractRecipient(File jsonFile) throws Exception {

        Set<String> recipients = new HashSet<>();
        JsonParser parser = Json.createParser(new FileInputStream(jsonFile));

        // instead of modelling context as stack, irrelevant branches are skipped
        while (parser.hasNext()) {
            final JsonParser.Event event = parser.next();
            switch (event) {
                case KEY_NAME: {
                    String name = parser.getString();
                    // skip parsing irrelevant branches
                    if (name.equals("cc") || name.equals("bcc")) {
                        parser.next();
                        parser.skipArray();
                    }
                    if (name.equals("from")) {
                        parser.next();
                        parser.skipObject();
                    }
                    // value found
                    else if (name.equals("displayname")){
                        // pick up value !
                        parser.next();
                        String value = parser.getString();
                        recipients.add(value);
                    }
                    break;
                }
            }
        }
        parser.close();

        return recipients;
    }
}

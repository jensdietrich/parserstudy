JsonReader rdr = ...
JsonObject root = rdr.readObject();
Set<String> recipients = root.getJsonArray("emails")
  .stream()
  .map(JsonObject.class::cast)
  .flatMap(email -> email.getJsonArray("to").stream())
  .map(JsonObject.class::cast)
  .map(to -> to.getString("displayname"))
  .collect(Collectors.toSet());
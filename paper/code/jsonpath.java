String pathQuery = "$.emails[*].to[*].displayname";
JSONArray results = JsonPath.parse(jsonFile)
	.read(pathQuery, JSONArray.class);
Set<String> recipients = results.stream()
    .map(String.class::cast)
    .collect(Collectors.toSet());
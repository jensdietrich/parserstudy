Set<String> recipients = new HashSet<>();
XPath xpath = ...
String pathQuery = "/emails/email/to/display_name";
XPathExpression expression = xpath.compile(pathQuery);
NodeList result = (NodeList)expression.evaluate(..);
for (int i=0;i<result.getLength();i++) {
    recipients.add(result.item(i).getTextContent());
}
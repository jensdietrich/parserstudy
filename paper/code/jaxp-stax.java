Set<String> recipients = new HashSet<>();
XMLEventReader eventReader = ...
Stack<String> context = new Stack<>();
while(eventReader.hasNext()){
  XMLEvent event = eventReader.nextEvent();
  if (event.isStartElement()) {
    context.push(event.asStartElement()
        .getName().getLocalPart());
  }
  else if (event.isEndElement()) {
    context.pop();
  }
  else if (event.isCharacters()) {
    // check context, if context is correct
    // extract name and add to recipients
  }
}


DocumentBuilder dBuilder = ...
Set<String> recipients = new HashSet<>(); 
Document doc = dBuilder.parse(xmlFile);
Element root = doc.getDocumentElement();
assert root.getTagName().equals("emails");
NodeList emailNodes = root.getChildNodes();
for (int j=0;j<emailNodes.getLength();j++) {
  if (emailNodes.item(j) instanceof Element) {
    Element emailEl = (Element)emailNodes.item(j);
    NodeList toNodes = emailEl.getElementsByTagName("to");
     for (int i = 0; i < toNodes.getLength(); i++) {
      Element toEl = (Element) toNodes.item(i);
      Element displayNameEl = (Element) toElement
        .getElementsByTagName("display_name").item(0);
      String displayName = displayNameEl.getTextContent();
      recipients.add(displayName);
    }
  }
}
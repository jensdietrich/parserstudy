Document doc = builder.build(xmlFile);
Element root = doc.getRootElement();
Set<String> recipients = root.getChildren("email").stream()
    .flatMap(to -> to.getChildren("to").stream())
    .flatMap(to -> to.getChildren("display_name").stream())
    .map(e -> e.getText())
    .collect(Collectors.toSet());
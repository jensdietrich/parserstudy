Set<String> recipients = new HashSet<>();
InputSource input = ...
DefaultHandler handler = new DefaultHandler() {
    private Stack<String> context = new Stack<>();
    public void startElement(.. String qName, ..){
      context.push(qName);
    }
    public void endElement(..){
      context.pop();
    }
    public void characters(char[] ch, int start, int l){
        // check context, if context is correct
        // extract name and add to recipients
    }
};
SAXParserFactory factory = SAXParserFactory.newInstance();
SAXParser saxParser = factory.newSAXParser();
saxParser.parse(input, handler);
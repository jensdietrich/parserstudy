<?xml version="1.0"?>
<callsites>
    <type>PATHQUERY</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>fastjson-path</name>
    <patterns>
        <pattern>
            <classname>com.alibaba.fastjson.JSONPath</classname>
            <methodname>compile</methodname>
        </pattern>
        <pattern>
            <classname>com.alibaba.fastjson.JSONPath</classname>
            <methodname>eval</methodname>
        </pattern>
    </patterns>
    <namespace>com.alibaba.fastjson</namespace>
</callsites>
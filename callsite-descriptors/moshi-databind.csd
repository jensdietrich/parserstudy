<?xml version="1.0"?>
<callsites>
    <type>DATABIND</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>moshi-bind</name>
    <patterns>
        <pattern>
            <classname>com.squareup.moshi.JsonAdapter</classname>
            <methodname>fromJson</methodname>
        </pattern>
    </patterns>
    <namespace>com.squareup.moshi</namespace>
    <!-- mosh also has a stax parser (JsonReader) but this cannot be publically instantiated -->
</callsites>


<?xml version="1.0"?>
<callsites>
    <type>PATHQUERY</type>
    <format>XML</format>
    <generated>false</generated>
    <name>xerces-path</name>
    <patterns>
        <pattern>
            <classname>org.apache.xerces.impl.xpath.XPath</classname>
        </pattern>
    </patterns>
    <namespace>org.apache.xerces</namespace>
</callsites>
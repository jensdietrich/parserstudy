<?xml version="1.0"?>
<callsites>
    <type>PATHQUERY</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>glassfish-path</name>
    <patterns>
        <pattern>
            <classname>org.glassfish.json.JsonProviderImpl</classname>
            <methodname>createPointer​</methodname>
        </pattern>
        <pattern>
            <classname>org.glassfish.json.JsonPointerImpl</classname>
        </pattern>
    </patterns>
    <namespace>org.glassfish.json</namespace>
</callsites>


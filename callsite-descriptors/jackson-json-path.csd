<?xml version="1.0"?>
<callsites>
    <type>PATHQUERY</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>jackson-path</name>
    <patterns>
        <pattern>
            <classname>com.fasterxml.jackson.databind.JsonNode</classname>
            <methodname>at</methodname>
        </pattern>
    </patterns>
    <namespace>com.fasterxml.jackson</namespace>
</callsites>


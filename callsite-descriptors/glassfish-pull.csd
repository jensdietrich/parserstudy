<?xml version="1.0"?>
<callsites>
    <type>PULL</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>glassfish-pull</name>
    <patterns>
        <pattern>
            <classname>org.glassfish.json.JsonProviderImpl</classname>
            <methodname>createParser</methodname>
        </pattern>
        <pattern>
            <classname>org.glassfish.json.JsonParserFactoryImpl</classname>
        </pattern>
        <pattern>
            <classname>org.glassfish.json.JsonParserImpl</classname>
        </pattern>
    </patterns>
    <namespace>org.glassfish.json</namespace>
</callsites>


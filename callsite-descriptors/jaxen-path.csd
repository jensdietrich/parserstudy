<?xml version="1.0"?>
<callsites>
    <type>PATHQUERY</type>
    <format>XML</format>
    <generated>false</generated>
    <name>jaxen-path</name>
    <patterns>
        <pattern>
            <classname>org.jaxen.XPath</classname>
            <methodname>evaluate</methodname>
        </pattern>
    </patterns>
    <namespace>org.jaxen</namespace>
</callsites>


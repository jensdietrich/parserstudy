<?xml version="1.0"?>
<callsites>
    <type>DOM</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>jackson-dom</name>
    <patterns>
        <pattern>
            <classname>com.fasterxml.jackson.databind.ObjectMapper</classname>
            <methodname>readTree</methodname>
        </pattern>
    </patterns>
    <namespace>com.fasterxml.jackson</namespace>
</callsites>


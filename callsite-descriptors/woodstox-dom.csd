<?xml version="1.0"?>
<callsites>
    <type>DOM</type>
    <format>XML</format>
    <generated>false</generated>
    <name>woodstox-dom</name>
    <patterns>
        <!-- this one is strange: it provide a stax API to access a DOM, but an intermediate DOM is still being built -->
        <pattern>
            <classname>com.ctc.wstx.dom.WstxDOMWrappingReader</classname>
        </pattern>
    </patterns>
    <namespace>com.ctc.wstx</namespace>
</callsites>


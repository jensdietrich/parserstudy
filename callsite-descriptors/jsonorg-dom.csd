<?xml version="1.0"?>
<callsites>
    <type>DOM</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>json.org-dom</name>
    <patterns>
        <pattern>
            <classname>org.json.JSONTokener</classname>
            <methodname>&lt;init&gt;</methodname>
        </pattern>
    </patterns>
    <namespace>org.json</namespace>
</callsites>


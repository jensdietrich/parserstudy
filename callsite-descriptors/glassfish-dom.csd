<?xml version="1.0"?>
<callsites>
    <type>DOM</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>glassfish-dom</name>
    <patterns>
        <pattern>
            <classname>org.glassfish.json.JsonProviderImpl</classname>
            <methodname>createReader</methodname>
        </pattern>
        <pattern>
            <classname>org.glassfish.json.JsonReaderFactoryImpl</classname>
        </pattern>
        <pattern>
            <classname>org.glassfish.json.JsonReaderImpl</classname>
        </pattern>
    </patterns>
    <namespace>org.glassfish.json</namespace>
</callsites>


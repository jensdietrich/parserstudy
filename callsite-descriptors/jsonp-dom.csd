<?xml version="1.0"?>
<callsites>
    <type>DOM</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>jsonp-dom</name>
    <patterns>
        <pattern>
            <classname>javax.json.Json</classname>
            <methodname>createReader</methodname>
        </pattern>
    </patterns>
    <namespace>javax.json</namespace>
</callsites>


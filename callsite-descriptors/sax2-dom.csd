<?xml version="1.0"?>
<callsites>
    <type>DOM</type>
    <format>XML</format>
    <generated>false</generated>
    <name>fasterxml-sax2-dom</name>
    <patterns>
        <!-- this one is strange: it provide a stax API to access a DOM, but an intermediate DOM is still being built -->
        <pattern>
            <classname>org.codehaus.stax2.ri.dom.DOMWrappingReader</classname>
        </pattern>
    </patterns>
    <namespace>org.codehaus.stax2</namespace>
</callsites>


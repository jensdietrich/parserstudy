<?xml version="1.0"?>
<callsites>
    <type>DOM</type>
    <format>XML</format>
    <generated>false</generated>
    <name>fasterxml-sax2-stax</name>
    <patterns>
        <pattern>
            <classname>org.codehaus.stax2.XMLEventReader2</classname>
        </pattern>
    </patterns>
    <namespace>org.codehaus.stax2</namespace>
</callsites>


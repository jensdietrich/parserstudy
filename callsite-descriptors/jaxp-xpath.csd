<?xml version="1.0"?>
<callsites>
    <type>PATHQUERY</type>
    <format>XML</format>
    <generated>false</generated>
    <name>jaxp-path</name>
    <patterns>
        <pattern>
            <classname>javax.xml.xpath.XPath</classname>
            <methodname>compile</methodname>
        </pattern>
        <pattern>
            <classname>javax.xml.xpath.XPath</classname>
            <methodname>evaluate</methodname>
        </pattern>
    </patterns>
    <namespace>javax.xml</namespace>
</callsites>


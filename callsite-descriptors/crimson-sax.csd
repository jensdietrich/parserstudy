<?xml version="1.0"?>
<callsites>
    <type>PUSH</type>
    <format>XML</format>
    <generated>false</generated>
    <name>crimson-push</name>
    <patterns>
        <pattern>
            <classname>org.apache.crimson.parser.Parser2</classname>
        </pattern>
        <pattern>
            <classname>org.apache.crimson.jaxp.SAXParserImpl</classname>
        </pattern>
        <pattern>
            <classname>org.apache.crimson.jaxp.SAXParserFactoryImpl</classname>
        </pattern>
    </patterns>
    <namespace>org.apache.crimson</namespace>
</callsites>


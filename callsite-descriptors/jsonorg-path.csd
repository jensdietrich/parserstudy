<?xml version="1.0"?>
<callsites>
    <type>PATHQUERY</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>json.org-path</name>
    <patterns>
        <pattern>
            <classname>org.json.JSONPointer</classname>
            <methodname>queryFrom</methodname>
        </pattern>
    </patterns>
    <namespace>org.json</namespace>
</callsites>


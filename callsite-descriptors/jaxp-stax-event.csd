<?xml version="1.0"?>
<callsites>
    <type>PULL</type>
    <format>XML</format>
    <generated>false</generated>
    <name>jaxp-pull</name>
    <patterns>
        <pattern>
            <classname>javax.xml.stream.XMLInputFactory</classname>
            <methodname>createXMLEventReader</methodname>
        </pattern>
    </patterns>
    <namespace>javax.xml</namespace>
</callsites>


package nz.ac.vuw.cs.parserstudy;

import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.Logger;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Analyser for jars -- finds parser callsites.
 * @author jens dietrich
 */
public class JarAnalyser {

    static final Logger LOGGER = LogSystem.getLogger(JarAnalyser.class);
    static final String COL_SEP = "\t";

    // TODO : implement main, parse args
    public static void main(String[] args) throws Exception {
        Options options = new Options();
        options.addOption("data", true, "The root folder containing the jars to be analysed")
                .addOption("out1",  true, "The detailed output file for the callsites found in the first versions(s) (CSV)")
                .addOption("out2",  true, "The detailed output file for the callsites found in the last versions(s) (CSV)");

        if (!options.hasOption("data") || !options.hasOption("out1") || !options.hasOption("out2")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java " + JarAnalyser.class.getName(), options);
        }

        CommandLine cmd = new DefaultParser().parse( options, args);

        String jarFolderName = cmd.getOptionValue("data");
        File jarFolder = new File(jarFolderName);
        Preconditions.checkArgument(jarFolder.exists());
        Preconditions.checkArgument(jarFolder.isDirectory());

        LOGGER.info("Analysing first versions");
        Collection<File> jars = VersionParser.getFirstVersions(jarFolder).stream() // Stream.of(jarFolder.listFiles())
            .filter(f -> f.getName().endsWith(".jar"))
            .collect(Collectors.toList());

        String outputFileName = cmd.getOptionValue("out1");
        File outputFile = new File(outputFileName);
        analyse(jars,outputFile);

        LOGGER.info("");
        LOGGER.info("Analysing last versions");
        jars = VersionParser.getLastVersions(jarFolder).stream() // Stream.of(jarFolder.listFiles())
                .filter(f -> f.getName().endsWith(".jar"))
                .collect(Collectors.toList());

        outputFileName = cmd.getOptionValue("out2");
        outputFile = new File(outputFileName);
        analyse(jars,outputFile);



    }


    public static void analyse(Collection<File> jars,File results) throws Exception {

        Collection<CallsiteDescriptor> callsiteDescriptors = CallsiteDescriptors.read();
        Preconditions.checkState(callsiteDescriptors.size()>0);
        LOGGER.info("" + callsiteDescriptors.size() + " imported");

        try (PrintStream detailOut = new PrintStream(new FileOutputStream(results));) {
            // first row is header
            printLine(detailOut,"jar","parsername","parsertype","format","classname","methodname","descriptor","parserinternal");
            int jarCounter = 0;
            for (File jar:jars) {
                AtomicInteger callsiteCounter = new AtomicInteger();
                CallsiteFinder.CallsiteFound callback = new CallsiteFinder.CallsiteFound() {
                    @Override
                    public void callsiteFound(String className, String methodName, String descriptor, CallsiteDescriptor callsiteDescriptor,boolean isParserInternalInvocation) {
                        printLine(detailOut,jar.getName(),callsiteDescriptor.getName(),callsiteDescriptor.getParserType().toString(),callsiteDescriptor.getFormat().toString(),className,methodName,descriptor,""+isParserInternalInvocation);
                    }
                };

                LOGGER.info("Analysing jar " + (++jarCounter) + "/" + jars.size() + " - " + jar.getName());
                long t = System.currentTimeMillis();
                new CallsiteFinder().analyse(jar,callsiteDescriptors,callback);
                LOGGER.info("\tdone, " + callsiteCounter.get() + " callsites found, this took " + (System.currentTimeMillis()-t) + " ms");
            }
        };
     }

    private static void printLine(PrintStream detailOut, String... values) {
        detailOut.println(Stream.of(values).collect(Collectors.joining(COL_SEP)));
    }

}

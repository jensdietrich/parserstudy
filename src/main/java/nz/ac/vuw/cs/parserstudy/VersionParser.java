package nz.ac.vuw.cs.parserstudy;

import com.google.common.base.Preconditions;
import com.google.common.collect.*;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Utility and data structure to represent versioned artifacts.
 * @author jens dietrich
 */
public class VersionParser {

    // for testing !
    public static void main (String[] args) throws Exception {
//        File folder = new File("/Users/jens/Development/mvn-data/artifacts");
//        AtomicInteger successCount = new AtomicInteger(0);
//        AtomicInteger failureCount = new AtomicInteger(0);
//        for (File jar:folder.listFiles(n -> n.getName().endsWith(".jar"))) {
//            try {
//                parse(jar.getName());
//                successCount.incrementAndGet();
//            }
//            catch (Exception x) {
//                System.out.println("Cannot parse " + jar.getName());
//                failureCount.incrementAndGet();
//            }
//        }
//
//        System.out.println("Done, success: " + successCount.get() + " , failure: " + failureCount.get() );

        // script to get cpunt of jar-pairs to report in paper
        File folder = new File("/Users/jens/Development/mvn-data/artifacts");
        Collection<Pair<File,File>> pairs = getFirstAndLastVersions(folder);
        System.out.println("" + pairs.size() + " first/last version jar pairs found");

    }

    public static class ArtifactVersion implements Comparable<ArtifactVersion>{
        public String name = null;
        public int major = -1;
        public int minor = -1;
        public int micro = -1;
        public String build = null;

        public ArtifactVersion(String name, int major, int minor, int micro,String build) {
            this.name = name;
            this.major = major;
            this.minor = minor;
            this.micro = micro;
            this.build = build;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ArtifactVersion that = (ArtifactVersion) o;

            if (major != that.major) return false;
            if (minor != that.minor) return false;
            if (micro != that.micro) return false;
            return name.equals(that.name);
        }

        @Override
        public int hashCode() {
            int result = name.hashCode();
            result = 31 * result + major;
            result = 31 * result + minor;
            result = 31 * result + micro;
            return result;
        }

        @Override
        public int compareTo(ArtifactVersion that) {
            Preconditions.checkNotNull(that);
            int diff = this.major - that.major;
            if (diff==0) {
                diff = this.minor - that.minor;
                if (diff==0) {
                    diff = this.micro - that.micro;
                    if (diff==0) {
                        String b1 = this.build==null?"":this.build;
                        String b2 = that.build==null?"":that.build;
                        return b1.compareTo(b2);
                    }
                }
            }
            return diff;
        }
    }

    public static final Predicate<String> VERSION_STRING_TEST = Pattern.compile("\\d(\\.\\d){0,2}(\\.[a-z,A-Z]\\w+)?").asPredicate();

    public static ArtifactVersion parse (String jar) {
        Preconditions.checkArgument(jar.endsWith(".jar"));
        jar = jar.substring(0,jar.length()-4);

        // some special cases -- chop of the respective suffixes
        String specialSuffix = null;
        if (jar.endsWith("-release") || jar.endsWith("-RELEASE") || jar.endsWith("-final") || jar.endsWith("-FINAL") || jar.endsWith("-jre")) {
            int idx = jar.lastIndexOf("-");
            specialSuffix = jar.substring(idx+1);
            jar = jar.substring(0,idx);
        }


        int idx = jar.lastIndexOf("-");
        Preconditions.checkArgument(idx>=0);
        String versionDef = jar.substring(idx+1);
        String name = jar.substring(0,idx);
        Preconditions.checkArgument(VERSION_STRING_TEST.test(versionDef));
        String[] versionTokens = versionDef.split("\\.");

        if (versionTokens.length==1) {
            return new ArtifactVersion(name,Integer.parseInt(versionTokens[0]),-1,-1,specialSuffix);
        }
        else if (versionTokens.length==2) {
            if (isNumeric(versionTokens[1])) {
                return new ArtifactVersion(name, Integer.parseInt(versionTokens[0]), Integer.parseInt(versionTokens[1]), -1,specialSuffix);
            }
            else {
                return new ArtifactVersion(name, Integer.parseInt(versionTokens[0]), -1, -1,versionTokens[1]);
            }
        }
        else if (versionTokens.length==3) {
            if (isNumeric(versionTokens[2])) {
                return new ArtifactVersion(name, Integer.parseInt(versionTokens[0]), Integer.parseInt(versionTokens[1]), Integer.parseInt(versionTokens[2]),specialSuffix);
            }
            else {
                return new ArtifactVersion(name, Integer.parseInt(versionTokens[0]), Integer.parseInt(versionTokens[1]), -1,versionTokens[2]);
            }
        }
        else if (versionTokens.length==4) {
            assert !isNumeric(versionTokens[3]);
            return new ArtifactVersion(name, Integer.parseInt(versionTokens[0]), Integer.parseInt(versionTokens[1]), Integer.parseInt(versionTokens[2]),versionTokens[3]);
        }
        assert false;
        return null;

    }

    public static Collection<Pair<File,File>> getFirstAndLastVersions(File folder) {
        Map<ArtifactVersion,File> files = new HashMap<>();
        Multimap<String,ArtifactVersion> versionsByName = TreeMultimap.create();
        Collection<Pair<File,File>> result = new HashSet<>();

        for (File jar:folder.listFiles(f -> f.getName().endsWith(".jar"))) {
            ArtifactVersion version = parse(jar.getName());
            files.put(version,jar);
            versionsByName.put(version.name,version);
        }

        for (String name:versionsByName.keys()) {
            List<ArtifactVersion> versions = Lists.newArrayList(versionsByName.get(name));
            // already sorted because tree map is used !
            assert versions.size() > 0;
            if (versions.size()==1) {
                System.out.println("warning, only one version found for artifact " + name);
            }
            ArtifactVersion firstVersion = versions.get(0);
            ArtifactVersion lastVersion = versions.get(versions.size()-1);
            Pair<File,File> nextPair = Pair.of(files.get(firstVersion),files.get(lastVersion));

            result.add(nextPair);
        }

        return result;
    }

    public static Collection<File> getFirstVersions(File folder) {
        return getFirstAndLastVersions(folder).stream()
            .map(p -> p.getLeft())
            .collect(Collectors.toSet());
    }

    public static Collection<File> getLastVersions(File folder) {
        return getFirstAndLastVersions(folder).stream()
            .map(p -> p.getRight())
            .collect(Collectors.toSet());
    }

    private static boolean isNumeric(String s) {
        for (char c:s.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }



}

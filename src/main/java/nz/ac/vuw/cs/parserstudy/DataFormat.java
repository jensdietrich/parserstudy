package nz.ac.vuw.cs.parserstudy;

/**
 * The data format.
 * @author jens dietrich
 */
public enum DataFormat { XML, JSON }

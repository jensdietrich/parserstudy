package nz.ac.vuw.cs.parserstudy;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

/**
 * An abstract callsite descriptor.
 * @author jens dietrich
 */
public class CallsiteDescriptor {

    public static class Pattern {
        private String className = null;
        private String methodName = null;
        private String descriptor = null;
        private Predicate<String> descriptorTest = d -> true;

        public Pattern(String className, String methodName, String descriptor) {
            this.className = className;
            this.methodName = methodName;
            this.descriptor = descriptor;
            if (this.descriptor!=null) {
                java.util.regex.Pattern regex = java.util.regex.Pattern.compile(this.descriptor);
                descriptorTest = regex.asPredicate();
            }
        }

        public String getClassName() {
            return className;
        }

        public String getMethodName() {
            return methodName;
        }

        public String getDescriptor() {
            return descriptor;
        }


        public boolean descriptorMatches(String desc) {
            return this.descriptorTest.test(desc);
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Pattern pattern = (Pattern) o;

            if (className != null ? !className.equals(pattern.className) : pattern.className != null) return false;
            if (methodName != null ? !methodName.equals(pattern.methodName) : pattern.methodName != null) return false;
            return descriptor != null ? descriptor.equals(pattern.descriptor) : pattern.descriptor == null;
        }

        @Override
        public int hashCode() {
            int result = className != null ? className.hashCode() : 0;
            result = 31 * result + (methodName != null ? methodName.hashCode() : 0);
            result = 31 * result + (descriptor != null ? descriptor.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "Pattern{" +
                    "className='" + className + '\'' +
                    ", methodName='" + methodName + '\'' +
                    ", descriptor='" + descriptor + '\'' +
                    '}';
        }
    }

    private ParserType parserType = null ;
    // whether the parser was generated via JAXB, Swagger etc
    private boolean generated = false;
    private Set<Pattern> patterns = new HashSet<>();
    private String name = null;
    private DataFormat format = null;
    private String parserNamespace = null;

    public CallsiteDescriptor(ParserType parserType, boolean generated, String name,DataFormat format,String parserNamespace) {
        this.parserType = parserType;
        this.generated = generated;
        this.name = name;
        this.format = format;
        this.parserNamespace = parserNamespace;
    }

    public ParserType getParserType() {
        return parserType;
    }

    public boolean isGenerated() {
        return generated;
    }

    public String getName() {
        return name;
    }

    public String getParserNamespace() {
        return parserNamespace;
    }

    public DataFormat getFormat() {
        return format;
    }

    public void addPattern(Pattern pattern) {
        this.patterns.add(pattern);
    }

    public Set<Pattern> getPatterns() {
        return patterns;
    }

    // whether the callsite in a class is parser internal
    public boolean isParserInternalInvocation(String nameOfClassWithCallsite) {
        return  nameOfClassWithCallsite.replace('/','.').startsWith(this.parserNamespace);
    }

    // whether this matches the callsite info found
    public boolean matches (String className,String methodName, String descriptor) {
        if (patterns.size()==0) {
            return true;
        }
        boolean matches = false;
        for (Pattern pattern:patterns) {
            matches = matches || matches(className,methodName,descriptor,pattern);
        }
        return matches;
    }

    private boolean matches (String className,String methodName, String descriptor, Pattern pattern) {
        boolean matches = true;

        matches = matches && (pattern.getClassName()==null || pattern.getClassName().equals(className));
        matches = matches && (pattern.getMethodName()==null || pattern.getMethodName().equals(methodName));
        matches = matches && pattern.descriptorMatches(descriptor);

        return matches;
    }


    @Override
    public String toString() {
        return "CallsiteDescriptor{" +
                "parserType=" + parserType +
                ", generated=" + generated +
                ", patterns=" + patterns +
                ", name='" + name + '\'' +
                ", format=" + format +
                ", parserNamespace='" + parserNamespace + '\'' +
                '}';
    }
}

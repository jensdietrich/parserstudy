package nz.ac.vuw.cs.parserstudy;

import com.google.common.base.Preconditions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Registry of available callsite descriptors, definitions are read from
 * file with the callsite extension in /callsites .
 * @author jens dietrich
 */
public class CallsiteDescriptors {

    private static Logger LOGGER = LogSystem.getLogger(CallsiteDescriptors.class);
    private static Set<CallsiteDescriptor> CALLSITES = null;

    public static Collection<CallsiteDescriptor> read () {
        synchronized (CallsiteDescriptors.class) {
            if (CALLSITES == null) {
                CALLSITES = new HashSet<>();
                File folder = new File("callsite-descriptors");
                Preconditions.checkState(folder.exists());
                Preconditions.checkState(folder.isDirectory());
                File[] defs = folder.listFiles((dir, name) -> name.endsWith(".csd"));
                LOGGER.info("Parsing " + defs.length + " callsite definitions");

                for (File def : defs) {
                    CallsiteDescriptor csd = parse(def);
                    CALLSITES.add(csd);
                }
            }
            return CALLSITES;
        }

    }

    public static CallsiteDescriptor parse (File def) {

        //        <?xml version="1.0"?>
        //        <callsites>
        //            <type>DOM</type>
        //            <format>XML</format>
        //            <generated>false</generated>
        //            <patterns>
        //                <pattern>
        //                    <classname>javax.xml.parsers.DocumentBuilder</classname>
        //                    <methodname>parse</methodname>
        //                    <descriptor>(Ljava.io.File;)Lorg.w3c.dom.Document;</descriptor>
        //                </pattern>
        //            </patterns>
        //        </callsites>

        try {

            LOGGER.info("reading callsite definition from " + def.getAbsolutePath());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(def);

            Element root = (Element)doc.getElementsByTagName("callsites").item(0);

            NodeList nodes = root.getElementsByTagName("type");
            assert nodes.getLength()==1;
            String typeS = nodes.item(0).getTextContent();
            ParserType type = ParserType.valueOf(typeS);

            nodes = root.getElementsByTagName("format");
            assert nodes.getLength()==1;
            String formatS = nodes.item(0).getTextContent();
            DataFormat format = DataFormat.valueOf(formatS);

            nodes = root.getElementsByTagName("name");
            assert nodes.getLength()==1;
            String name = nodes.item(0).getTextContent();

            nodes = root.getElementsByTagName("generated");
            assert nodes.getLength()==1;
            String generatedS = nodes.item(0).getTextContent();
            boolean generated = Boolean.valueOf(generatedS);

            nodes = root.getElementsByTagName("namespace");
            assert nodes.getLength()==1;
            String namespace = nodes.item(0).getTextContent();

            nodes = root.getElementsByTagName("patterns");
            assert nodes.getLength()==1;
            Element patternsNode = (Element) nodes.item(0);
            nodes = patternsNode.getElementsByTagName("pattern");

            CallsiteDescriptor callsite = new CallsiteDescriptor(type,generated,name,format,namespace);

            for (int i=0;i<nodes.getLength();i++) {

                Element patternNode = (Element) nodes.item(i);

                NodeList nodes2 = patternNode.getElementsByTagName("classname");
                String className = null;
                if (nodes2.getLength() > 0) {
                    assert nodes2.getLength()==1;
                    //  map class names to use bytecode / ASM notation
                    className = nodes2.item(0).getTextContent().replace('.','/');
                }

                nodes2 = patternNode.getElementsByTagName("methodname");
                String methodname = null;
                if (nodes2.getLength() > 0) {
                    assert nodes2.getLength() == 1;
                    methodname = nodes2.item(0).getTextContent();
                }

                //  map class names to use bytecode / ASM notation
                nodes2 = patternNode.getElementsByTagName("descriptor");
                String descriptor = null;
                if (nodes2.getLength() > 0) {
                    assert nodes2.getLength() == 1;
                    descriptor = nodes2.item(0).getTextContent().replace('.','/');;
                }

                CallsiteDescriptor.Pattern pattern = new CallsiteDescriptor.Pattern(className,methodname,descriptor);
                callsite.addPattern(pattern);
            }


            return callsite;

        }
        catch (Exception x) {
            throw new RuntimeException("Cannot parse " + def.getAbsolutePath(),x);
        }
    }
}

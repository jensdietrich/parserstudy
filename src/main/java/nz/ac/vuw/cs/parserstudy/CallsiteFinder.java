package nz.ac.vuw.cs.parserstudy;

import org.apache.logging.log4j.Logger;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import java.io.File;
import java.io.InputStream;
import java.util.Collection;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Utility to parse a jar file for the occurance of certain APIs, and export the finding to
 * a CSV file.
 * @author jens dietrich
 */
public class CallsiteFinder {

    private static Logger LOGGER = LogSystem.getLogger(CallsiteFinder.class);

    public interface CallsiteFound {
        /**
         * Callback that a callsite has been detected.
         * the parameters indicate where the callsite has been found, and what pattern describing the callsite
         * @param className
         * @param methodName
         * @param descriptor
         * @param callsiteDescriptor
         * @param isParserInternal
         */
        void callsiteFound(String className,String methodName,String descriptor,CallsiteDescriptor callsiteDescriptor,boolean isParserInternal);
    }

    public static class CallsiteCollector extends ClassVisitor {

        static private final int ASM = Opcodes.ASM7;
        private CallsiteFound callback = null;
        private Collection<CallsiteDescriptor> callsiteDescriptors = null;
        private String visitedClassName = null;

        public CallsiteCollector(Collection<CallsiteDescriptor> callsiteDescriptors,CallsiteFound callback) {
            super(ASM);
            this.callsiteDescriptors = callsiteDescriptors;
            this.callback = callback;
        }

        @Override
        public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
            super.visit(version, access, name, signature, superName, interfaces);
            this.visitedClassName = name;
        }

        @Override
        public MethodVisitor visitMethod(int access, String methodName, String methodDescriptor, String signature, String[] exceptions) {
            return new MethodVisitor(ASM) {
                @Override
                public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
                    for (CallsiteDescriptor callsiteDescriptor:callsiteDescriptors) {
                        if (callsiteDescriptor.matches(owner,name,descriptor)) {
                            boolean  isParserInternalInvocation =  callsiteDescriptor.isParserInternalInvocation(visitedClassName);
                            callback.callsiteFound(visitedClassName,methodName,methodDescriptor,callsiteDescriptor,isParserInternalInvocation);
                        }
                    }
                }
            };
        }
    }

    public void analyse (InputStream bytecode,Collection<CallsiteDescriptor> callsiteDescriptors,CallsiteFound callback) {
        try {
            CallsiteCollector collector = new CallsiteCollector(callsiteDescriptors, callback);
            new ClassReader(bytecode).accept(collector, 0);
        }
        catch (Exception x) {
            throw new RuntimeException("Problem parsing bytecode",x);
        }
    }

    public void analyse (File jar, Collection<CallsiteDescriptor> callsiteDescriptors, CallsiteFound callback) throws Exception {
        ZipFile zip = new ZipFile(jar);
        Enumeration<? extends ZipEntry> en = zip.entries();
        while (en.hasMoreElements()) {
            ZipEntry e = en.nextElement();
            String name = e.getName();
            if (name.endsWith(".class")) {
                InputStream in = zip.getInputStream(e);
                analyse(in,callsiteDescriptors,callback);
                in.close();
            }
        }
    }
}

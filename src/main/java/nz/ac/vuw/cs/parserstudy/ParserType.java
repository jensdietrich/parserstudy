package nz.ac.vuw.cs.parserstudy;

/**
 * The type of parser.
 * @author jens dietrich
 */
public enum ParserType { DOM, PULL, PUSH, DATABIND, PATHQUERY, OTHER }

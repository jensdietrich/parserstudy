package nz.ac.vuw.cs.parserstudy;

import com.google.common.base.Preconditions;
import org.apache.commons.cli.*;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.Logger;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Script to analyse how the type of parser changes between the first and the last version in range.
 * @author jens dietrich
 */
public class ParserUsageEvolutionAnalyser {

    static final Logger LOGGER = LogSystem.getLogger(ParserUsageEvolutionAnalyser.class);
    static final String COL_SEP = "\t";

    // TODO : implement main, parse args
    public static void main(String[] args) throws Exception {
        Options options = new Options();
        options.addOption("data", true, "The root folder containing the jars to be analysed")
                .addOption("jsondetailsout",true,"json details will be logged to this file")
                .addOption("xmldetailsout",true,"json details will be logged to this file")
                .addOption("first",  true, "The detailed output file for the callsites found in the first versions(s) (CSV), produced by JarAnalyser")
                .addOption("last",  true, "The detailed output file for the callsites found in the last versions(s) (CSV), produced by JarAnalyser")
                .addOption("jsonout",true,"the latex file with json results")
                .addOption("xmlout",true,"the latex file with xml results");

        if (    !options.hasOption("data") ||
                !options.hasOption("first") ||
                !options.hasOption("last") ||
                !options.hasOption("xmldetailsout") ||
                !options.hasOption("jsondetailsout") ||
                !options.hasOption("jsonout") ||
                !options.hasOption("xmlout"))
        {

            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java " + JarAnalyser.class.getName(), options);
        }

        CommandLine cmd = new DefaultParser().parse( options, args);

        String jarFolderName = cmd.getOptionValue("data");
        File jarFolder = new File(jarFolderName);
        Preconditions.checkArgument(jarFolder.exists());
        Preconditions.checkArgument(jarFolder.isDirectory());

        File jsonDetailsOut = new File(cmd.getOptionValue("jsondetailsout"));
        File xmlDetailsOut = new File(cmd.getOptionValue("xmldetailsout"));
        File jsonSummaryOut = new File(cmd.getOptionValue("jsonout"));
        File xmlSummaryOut = new File(cmd.getOptionValue("xmlout"));

        LOGGER.info("Analysing evolution -- XML");

        // prepare, parse CVS
        Map<String,EnumSet<ParserType>> xmlParserTypesUsedInFirstVersion = parseAnalysisResults(cmd.getOptionValue("first"),"XML");
        Map<String,EnumSet<ParserType>> xmlParserTypesUsedInLastVersion = parseAnalysisResults(cmd.getOptionValue("last"),"XML");

        Map<String,EnumSet<ParserType>> jsonParserTypesUsedInFirstVersion = parseAnalysisResults(cmd.getOptionValue("first"),"JSON");
        Map<String,EnumSet<ParserType>> jsonParserTypesUsedInLastVersion = parseAnalysisResults(cmd.getOptionValue("last"),"JSON");

        Map<String,Integer> xmlChanges = new TreeMap<>();
        Map<String,Integer> jsonChanges = new TreeMap<>();
        List<String> xmlChangesDetails = new ArrayList<>();
        List<String> jsonChangesDetails = new ArrayList<>();

        for (Pair<File,File> firstAndLast:VersionParser.getFirstAndLastVersions(jarFolder).stream().collect(Collectors.toList())) {
            String first = firstAndLast.getLeft().getName();
            String last = firstAndLast.getRight().getName();
            if (!first.equals(last)) {
                EnumSet<ParserType> xmlParseTypesUsedInFirst = xmlParserTypesUsedInFirstVersion.getOrDefault(first,EnumSet.noneOf(ParserType.class));
                EnumSet<ParserType> xmlParseTypesUsedInLast = xmlParserTypesUsedInLastVersion.getOrDefault(last,EnumSet.noneOf(ParserType.class));
                if (!xmlParseTypesUsedInFirst.equals(xmlParseTypesUsedInLast)) {
                    String changeString = computeChangeString(xmlParseTypesUsedInFirst,xmlParseTypesUsedInLast);
                    xmlChanges.compute(changeString, (k,v) -> v==null?1:v+1);
                    xmlChangesDetails.add(first + COL_SEP + last + COL_SEP + changeString);
                }

                EnumSet<ParserType> jsonParseTypesUsedInFirst = jsonParserTypesUsedInFirstVersion.getOrDefault(first,EnumSet.noneOf(ParserType.class));
                EnumSet<ParserType> jsonParseTypesUsedInLast = jsonParserTypesUsedInLastVersion.getOrDefault(last,EnumSet.noneOf(ParserType.class));
                if (!jsonParseTypesUsedInFirst.equals(jsonParseTypesUsedInLast)) {
                    String changeString = computeChangeString(jsonParseTypesUsedInFirst,jsonParseTypesUsedInLast);
                    jsonChanges.compute(changeString, (k,v) -> v==null?1:v+1);
                    jsonChangesDetails.add(first + COL_SEP + last + COL_SEP + changeString);
                }

            }
        }

        // sort
        List<String> sortedXMLChanges = new ArrayList<>();
        sortedXMLChanges.addAll(xmlChanges.keySet());
        Collections.sort(sortedXMLChanges, (ch1,ch2) -> xmlChanges.get(ch2)-xmlChanges.get(ch1));

        List<String> sortedJSONChanges = new ArrayList<>();
        sortedJSONChanges.addAll(jsonChanges.keySet());
        Collections.sort(sortedJSONChanges, (ch1,ch2) -> jsonChanges.get(ch2)-jsonChanges.get(ch1));

        // export details
        Files.write(jsonDetailsOut.toPath(),jsonChangesDetails, Charset.defaultCharset());
        LOGGER.info("JSON parser change details written to " + jsonDetailsOut.getAbsolutePath());
        Files.write(xmlDetailsOut.toPath(),xmlChangesDetails, Charset.defaultCharset());
        LOGGER.info("XML parser change details written to " + xmlDetailsOut.getAbsolutePath());

        // export summary as latex
        int SHOW_RECORDS = 15;

        try (PrintWriter out = new PrintWriter(new FileWriter(xmlSummaryOut))) {
            out.println("\\begin{tabular}{|ll|}");
            out.println("\t\\hline");
            out.println("\tchange pattern\t&\tcount \\\\");
            out.println("\t\\hline");
            sortedXMLChanges.stream().limit(SHOW_RECORDS).
                forEach(change -> out.println("\t" + change + "\t&\t" + xmlChanges.get(change) + "\\\\")
            );
            out.println("\t\\hline");
            out.println("\\end{tabular}");
            LOGGER.info("XML parser change summary written to " + xmlSummaryOut.getAbsolutePath());
        }

        try (PrintWriter out = new PrintWriter(new FileWriter(jsonSummaryOut))) {
            out.println("\\begin{tabular}{|ll|}");
            out.println("\t\\hline");
            out.println("\tchange pattern\t&\tcount \\\\");
            out.println("\t\\hline");
            sortedJSONChanges.stream().limit(SHOW_RECORDS).
                    forEach(change -> out.println("\t" + change + "\t&\t" + jsonChanges.get(change) + "\\\\")
                    );
            out.println("\t\\hline");
            out.println("\\end{tabular}");
            LOGGER.info("JSON parser change summary written to " + jsonSummaryOut.getAbsolutePath());
        }
    }


    private static String computeChangeString(EnumSet<ParserType> types1, EnumSet<ParserType> types2) {
        List<String> changes = new ArrayList<>();

        for (ParserType type:ParserType.values()) {
            if (types1.contains(type) && !types2.contains(type)) {
                changes.add("-" + type);
            }
        }

        for (ParserType type:ParserType.values()) {
            if (!types1.contains(type) && types2.contains(type)) {
                changes.add("+" + type);
            }
        }

        String extension = "";
        if (types1.isEmpty() && !types2.isEmpty()) {
            extension = " (+)";
        }
        else if (types2.isEmpty() && !types1.isEmpty()) {
            extension = " (-)";
        }

        return changes.stream().collect(Collectors.joining(",")) + extension;
    }

    private static Map<String, EnumSet<ParserType>> parseAnalysisResults(String csvName,String format) throws Exception{
        File csv = new File(csvName);
        Preconditions.checkArgument(csv.exists());
        Map<String,EnumSet<ParserType>> parserTypesByJar = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(csv))) {
            String line = null;
            boolean first = true;
            while ((line = reader.readLine())!=null) {
                if (first) {
                    // skip, this is the header
                    first = false;
                }
                else {
                    String[] tokens = line.split(COL_SEP);
                    if (format.equals(tokens[3])) {
                        String jar = tokens[0];
                        ParserType type = ParserType.valueOf(tokens[2]);
                        parserTypesByJar.compute(jar, (k, v) -> {
                            if (v == null) {
                                return EnumSet.of(type);
                            } else {
                                v.add(type);
                                return v;
                            }
                        });
                    }
                }
            }
        }

        return parserTypesByJar;
    }


}

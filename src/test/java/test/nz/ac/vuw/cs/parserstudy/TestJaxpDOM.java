package test.nz.ac.vuw.cs.parserstudy;

/**
 * Test case to recognise the use of JAXP-DOM parsing.
 * @author jens dietrich
 */
public class TestJaxpDOM extends AbstractTest {

    @Override
    protected String getJarFileName() {
        return "examples/jaxp-dom/target/examples-jaxpdom-1.0-SNAPSHOT.jar";
    }

    @Override
    protected String getCallsiteName() {
        return "jaxp-dom";
    }

    @Override
    protected MethodSpec getExpectedMethodWithCallsite() {
        return new MethodSpec("nz/ac/vuw/cs/parserstudy/jaxpdom/Example","main","([Ljava/lang/String;)V");
    }

}

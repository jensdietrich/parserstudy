package test.nz.ac.vuw.cs.parserstudy;

import nz.ac.vuw.cs.parserstudy.VersionParser;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;
import java.io.File;
import java.util.Collection;
import static org.junit.Assert.*;

public class TestVersionParser {

    @Test
    public void testParse1 () {
        VersionParser.ArtifactVersion version = VersionParser.parse("foo-1.2.3.jar");
        assertEquals("foo",version.name);
        assertEquals(1,version.major);
        assertEquals(2,version.minor);
        assertEquals(3,version.micro);
        assertNull(version.build);
    }

    @Test
    public void testParse2 () {
        VersionParser.ArtifactVersion version = VersionParser.parse("foo-10.20.30.jar");
        assertEquals("foo",version.name);
        assertEquals(10,version.major);
        assertEquals(20,version.minor);
        assertEquals(30,version.micro);
        assertNull(version.build);
    }

    @Test
    public void testParse3 () {
        VersionParser.ArtifactVersion version = VersionParser.parse("foo-1.2.jar");
        assertEquals("foo",version.name);
        assertEquals(1,version.major);
        assertEquals(2,version.minor);
        assertEquals(-1,version.micro);
        assertNull(version.build);
    }

    @Test
    public void testParse4 () {
        VersionParser.ArtifactVersion version = VersionParser.parse("foo-42.jar");
        assertEquals("foo",version.name);
        assertEquals(42,version.major);
        assertEquals(-1,version.minor);
        assertEquals(-1,version.micro);
        assertNull(version.build);
    }

    @Test
    public void testParse5 () {
        VersionParser.ArtifactVersion version = VersionParser.parse("foo-bar-1.2.3.jar");
        assertEquals("foo-bar",version.name);
        assertEquals(1,version.major);
        assertEquals(2,version.minor);
        assertEquals(3,version.micro);
        assertNull(version.build);
    }

    @Test
    public void testParse6 () {
        VersionParser.ArtifactVersion version = VersionParser.parse("foo-1.2.3.GA1.jar");
        assertEquals("foo",version.name);
        assertEquals(1,version.major);
        assertEquals(2,version.minor);
        assertEquals(3,version.micro);
        assertEquals("GA1",version.build);
    }

    @Test
    public void testParse7 () {
        VersionParser.ArtifactVersion version = VersionParser.parse("foo-1.2.GA1.jar");
        assertEquals("foo",version.name);
        assertEquals(1,version.major);
        assertEquals(2,version.minor);
        assertEquals(-1,version.micro);
        assertEquals("GA1",version.build);
    }

    @Test
    public void testParse8 () {
        VersionParser.ArtifactVersion version = VersionParser.parse("foo-1.GA1.jar");
        assertEquals("foo",version.name);
        assertEquals(1,version.major);
        assertEquals(-1,version.minor);
        assertEquals(-1,version.micro);
        assertEquals("GA1",version.build);
    }

    @Test
    public void testParse9 () {
        VersionParser.ArtifactVersion version = VersionParser.parse("foo-1.2.3-FINAL.jar");
        assertEquals("foo",version.name);
        assertEquals(1,version.major);
        assertEquals(2,version.minor);
        assertEquals(3,version.micro);
        assertEquals("FINAL",version.build);
    }

    @Test
    public void testParse10 () {
        VersionParser.ArtifactVersion version = VersionParser.parse("foo-1.2-release.jar");
        assertEquals("foo",version.name);
        assertEquals(1,version.major);
        assertEquals(2,version.minor);
        assertEquals(-1,version.micro);
        assertEquals("release",version.build);
    }

    @Test
    public void testParse11 () {
        VersionParser.ArtifactVersion version = VersionParser.parse("foo-1-jre.jar");
        assertEquals("foo",version.name);
        assertEquals(1,version.major);
        assertEquals(-1,version.minor);
        assertEquals(-1,version.micro);
        assertEquals("jre",version.build);
    }

    @Test
    public void testVersionRanges () {
        File dir = new File("src/test/resources");
        Collection<Pair<File,File>> firstAndLastVersions = VersionParser.getFirstAndLastVersions(dir);
        assertEquals(2,firstAndLastVersions.size());

        // oracles
        Pair<File,File> push = Pair.of(new File(dir,"push-20.jar"),new File(dir,"push-29.jar"));
        Pair<File,File> toothpick = Pair.of(new File(dir,"toothpick-2.0.0.jar"),new File(dir,"toothpick-3.0.2.jar"));

        assertTrue(firstAndLastVersions.contains(push));
        assertTrue(firstAndLastVersions.contains(toothpick));
    }

    @Test
    public void testFirstVersions () {
        File dir = new File("src/test/resources");
        Collection<File> firstVersions = VersionParser.getFirstVersions(dir);
        assertEquals(2,firstVersions.size());
        assertTrue(firstVersions.contains(new File(dir,"push-20.jar")));
        assertTrue(firstVersions.contains(new File(dir,"toothpick-2.0.0.jar")));
    }

    @Test
    public void testLastVersions () {
        File dir = new File("src/test/resources");
        Collection<File> firstVersions = VersionParser.getLastVersions(dir);
        assertEquals(2,firstVersions.size());
        assertTrue(firstVersions.contains(new File(dir,"push-29.jar")));
        assertTrue(firstVersions.contains(new File(dir,"toothpick-3.0.2.jar")));
    }

}

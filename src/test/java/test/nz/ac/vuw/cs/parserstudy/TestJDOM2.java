package test.nz.ac.vuw.cs.parserstudy;

/**
 * Test case to recognise the use of JDOM2 parsing.
 * @author jens dietrich
 */
public class TestJDOM2 extends AbstractTest {

    @Override
    protected String getJarFileName() {
        return "examples/jdom2/target/examples-jdom2-1.0-SNAPSHOT.jar";
    }

    @Override
    protected String getCallsiteName() {
        return "jdom2-dom";
    }

    @Override
    protected MethodSpec getExpectedMethodWithCallsite() {
        return new MethodSpec("nz/ac/vuw/cs/parserstudy/jdom2/Example","main","([Ljava/lang/String;)V");
    }

}

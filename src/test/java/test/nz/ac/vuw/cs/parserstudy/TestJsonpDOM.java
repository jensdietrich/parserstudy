package test.nz.ac.vuw.cs.parserstudy;

/**
 * Test case to recognise the use of JSONP-DOM parsing.
 * @author jens dietrich
 */
public class TestJsonpDOM extends AbstractTest {

    @Override
    protected String getJarFileName() {
        return "examples/jsonp-dom/target/examples-jsonpdom-1.0-SNAPSHOT.jar";
    }

    @Override
    protected String getCallsiteName() {
        return "jsonp-dom";
    }

    @Override
    protected MethodSpec getExpectedMethodWithCallsite() {
        return new MethodSpec("nz/ac/vuw/cs/parserstudy/jsonpdom/Example","main","([Ljava/lang/String;)V");
    }

}

package test.nz.ac.vuw.cs.parserstudy;


/**
 * Test case to recognise the use of JAXP-DOM parsing.
 * @author jens dietrich
 */
public class TestJaxpSTAX extends AbstractTest {

    @Override
    protected String getJarFileName() {
        return "examples/jaxp-stax/target/examples-jaxpstax-1.0-SNAPSHOT.jar";
    }

    @Override
    protected String getCallsiteName() {
        return "jaxp-pull";
    }

    @Override
    protected MethodSpec getExpectedMethodWithCallsite() {
        return new MethodSpec("nz/ac/vuw/cs/parserstudy/jaxpstax/Example","main","([Ljava/lang/String;)V");
    }
}

package test.nz.ac.vuw.cs.parserstudy;


/**
 * Test case to recognise the use of JAXP-DOM parsing.
 * @author jens dietrich
 */
public class TestJaxpSAX extends AbstractTest {

    @Override
    protected String getJarFileName() {
        return "examples/jaxp-sax/target/examples-jaxpsax-1.0-SNAPSHOT.jar";
    }

    @Override
    protected String getCallsiteName() {
        return "jaxp-push";
    }

    @Override
    protected AbstractTest.MethodSpec getExpectedMethodWithCallsite() {
        return new AbstractTest.MethodSpec("nz/ac/vuw/cs/parserstudy/jaxpsax/Example","main","([Ljava/lang/String;)V");
    }
}

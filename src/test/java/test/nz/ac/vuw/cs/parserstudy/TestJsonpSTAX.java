package test.nz.ac.vuw.cs.parserstudy;

/**
 * Test case to recognise the use of JSONP-DOM parsing.
 * @author jens dietrich
 */
public class TestJsonpSTAX extends AbstractTest {

    @Override
    protected String getJarFileName() {
        return "examples/jsonp-stax/target/examples-jsonpstax-1.0-SNAPSHOT.jar";
    }

    @Override
    protected String getCallsiteName() {
        return "jsonp-pull";
    }

    @Override
    protected MethodSpec getExpectedMethodWithCallsite() {
        return new MethodSpec("nz/ac/vuw/cs/parserstudy/jsonpstax/Example","main","([Ljava/lang/String;)V");
    }

}

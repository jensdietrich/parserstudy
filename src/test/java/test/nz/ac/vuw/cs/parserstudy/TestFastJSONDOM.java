package test.nz.ac.vuw.cs.parserstudy;

/**
 * Test case to recognise the use of FASTJSON-DOM parsing.
 * @author jens dietrich
 */
public class TestFastJSONDOM extends AbstractTest {

    @Override
    protected String getJarFileName() {
        return "examples/fastjson/target/examples-fastjson-1.0-SNAPSHOT.jar";
    }

    @Override
    protected String getCallsiteName() {
        return "fastjson-dom";
    }

    @Override
    protected MethodSpec getExpectedMethodWithCallsite() {
        return new MethodSpec("nz/ac/vuw/cs/parserstudy/fastjson/Example","main","([Ljava/lang/String;)V");
    }

}

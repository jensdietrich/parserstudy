package test.nz.ac.vuw.cs.parserstudy;

/**
 * Test case to recognise the use of DOM4J parsing.
 * @author jens dietrich
 */
public class TestDOM4J extends AbstractTest {

    @Override
    protected String getJarFileName() {
        return "examples/dom4j/target/examples-dom4j-1.0-SNAPSHOT.jar";
    }

    @Override
    protected String getCallsiteName() {
        return "dom4j-dom";
    }

    @Override
    protected MethodSpec getExpectedMethodWithCallsite() {
        return new MethodSpec("nz/ac/vuw/cs/parserstudy/dom4j/Example","main","([Ljava/lang/String;)V");
    }

}

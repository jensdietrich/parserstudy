package test.nz.ac.vuw.cs.parserstudy;

import nz.ac.vuw.cs.parserstudy.CallsiteDescriptor;
import nz.ac.vuw.cs.parserstudy.CallsiteDescriptors;
import nz.ac.vuw.cs.parserstudy.CallsiteFinder;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

/**
 * Abstract test.
 * @author jens dietrich
 */
public abstract class AbstractTest {

    public static class MethodSpec {
        public MethodSpec(String className, String methodName, String descriptor) {
            this.className = className;
            this.methodName = methodName;
            this.descriptor = descriptor;
        }

        public String className = null;
        public String methodName = null;
        public String descriptor = null;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            MethodSpec that = (MethodSpec) o;

            if (className != null ? !className.equals(that.className) : that.className != null) return false;
            if (methodName != null ? !methodName.equals(that.methodName) : that.methodName != null) return false;
            return descriptor != null ? descriptor.equals(that.descriptor) : that.descriptor == null;
        }

        @Override
        public int hashCode() {
            int result = className != null ? className.hashCode() : 0;
            result = 31 * result + (methodName != null ? methodName.hashCode() : 0);
            result = 31 * result + (descriptor != null ? descriptor.hashCode() : 0);
            return result;
        }
    }

    protected abstract String getJarFileName() ;
    protected abstract String getCallsiteName() ;
    protected abstract MethodSpec getExpectedMethodWithCallsite();

    protected File jar = null;
    protected Collection<CallsiteDescriptor> callsiteDescriptors = null;
    protected CallsiteDescriptor callsiteDescriptor = null;

    @Before
    public void setup() {
        this.jar = new File(this.getJarFileName());
        this.callsiteDescriptors = CallsiteDescriptors.read();
        this.callsiteDescriptor = this.callsiteDescriptors.stream()
            .filter(csd -> csd.getName().equals(this.getCallsiteName()))
            .findFirst()
            .get();
    }

    @Test
    public void test() throws Exception {
        Assume.assumeTrue("Jar not found: " + jar.getAbsolutePath() + " --  example project must be built with Maven first to create jar",this.jar.exists());
        Assume.assumeTrue(this.callsiteDescriptor!=null);
        AtomicInteger callbackCounter = new AtomicInteger();
        MethodSpec method = this.getExpectedMethodWithCallsite();
        CallsiteFinder.CallsiteFound callback = new CallsiteFinder.CallsiteFound() {
            @Override
            public void callsiteFound(String className, String methodName, String descriptor, CallsiteDescriptor callsiteDescriptor,boolean isParserInternalInvocation) {
                assertSame(AbstractTest.this.callsiteDescriptor,callsiteDescriptor);
                assertEquals(method.className,className);
                assertEquals(method.methodName,methodName);
                assertEquals(method.descriptor,descriptor);
                callbackCounter.incrementAndGet();
            }
        };
        new CallsiteFinder().analyse(jar,callsiteDescriptors,callback);
        assertEquals(1,callbackCounter.get());
    }


}

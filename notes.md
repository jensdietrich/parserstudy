# Notes



## List of APIs to Support

## methodology

1. mvnrepository.com search for xml
2. mvnrepository.com search for json

## low-level parsing
1. jaxp -- done
2. jsonp -- done
3. xerces xerces » xercesImpl
4. crimson
5. javax.xml.stream » stax-api

## dom APIs

1. dom4j  -- done
2. jdom
3. jdom2  -- done
4. json.org
5. fastjson

## data binding 

1. jaxb --javax.xml.bind » jaxb-api  and com.sun.xml.bind » jaxb-impl
2. jackson -- com.fasterxml.jackson.dataformat » jackson-dataformat-xml 
3. swagger generated api ?
4. XML Serialiser / Deserialiser
5. org.codehaus.castor » castor-xml
6. jakarta.xml.bind » jakarta.xml.bind-api
7. org.jibx » jibx-run
8. see https://stackoverflow.com/questions/27984619/what-are-the-api-that-does-implement-jsr-353-json for json related specs incl. data binding

## path APIs

1. jsonpath 
2. jaxen ? (*is this used directly ?*)
3. jxpath ? (*is this used directly ?*)
4. json pointer, supported by json.org and jackson, see https://github.com/google/gson/issues/1443 

## unclassified

1. xml-resolver » xml-resolver
2. org.simpleframework » simple-xml
3. com.fasterxml » aalto-xml
4. xml-apis » xml-apis-ext
5. com.helger » ph-xml
6. org.mule.modules » mule-module-xml
7. com.jcabi » jcabi-xml
8. com.ximpleware » vtd-xml 
9. com.fasterxml.woodstox » woodstox-core  provider for stax and sax apis

## not considered due to low usage

1. org.cdmckay.coffeedom » coffeedom  -- JDOM clone, only 3 usages in mvnrepository.com

## ignored, non-Java

1. org.scala-lang.modules » scala-xml
2. org.codehaus.groovy » groovy-xml 
3. org.ballerinalang » xml
4. org.clojure » data.xml

## ignored, frameworks

1. javax.xml.soap » javax.xml.soap-api

## others

1. com.atlassian.security » atlassian-secure-xml  -- creates standard jaxp - compliant parser factories 

## Analysing Android (APKs)

### Dataset

1. see Geiger, Franz-Xaver, and Ivano Malavolta. "Datasets of Android Applications: a Literature Review." arXiv preprint arXiv:1809.10069 (2018).
2. AndroZoo -- huge 10m+ , need to apply for access code
3. Can use web service to download APKs
4. F-Droid free -- but would be better to use general applications since this contains commercial ones. Are there legal issues ? 

### Analysis

1. download apktool, then run `java -jar apktool.jar d <appname>.apk`
2. creates file structure with decompiled files in smali format in `smali` folder
3. library classes are part of this

## other things to study

1. xml validation
2. generic line-based processing (CSV & co), e.g. study STAX-like `BufferedReader::readLine` vs (guava ? apache commons io ?) APIs to read all lines into a list (DOM-like). Full modelling of loops to check whether 
the entire file is loaded is complicated and probably impossible to do acuratly (guard conditions to break would need to be modelled), but we would probably get away with not doing this as discussing the limitations. 

## Related Work

1. Deshmukh, Ms VM, and Dr GR Bamnote. "An Empirical Study: XML Parsing using Various Data Structures." International Journal Of Computer Science And Applications 6.2 (2013): 0974-1011.  *not particulary well-done, but should cite* 
2. Oliveira, Bruno, Vasco Santos, and Orlando Belo. "Processing XML with Java–a performance benchmark." International Journal of New Computer Architectures and their Applications (IJNCAA) 3.1 (2013): 72-85.
3. Deshmukh, Vaishali M., and G. R. Bamnote. "An Empirical Study of XML Parsers across Applications." 2015 International Conference on Computing Communication Control and Automation. IEEE, 2015.
4. Wu, DaYong, et al. "A comparative study on performance of XML parser APIs (DOM and SAX) in parsing efficiency." Proceedings of the 3rd International Conference on Cryptography, Security and Privacy. ACM, 2019.
5. Schmidt, Albrecht, et al. "XMark: A benchmark for XML data management." VLDB'02: Proceedings of the 28th International Conference on Very Large Databases. Morgan Kaufmann, 2002.  **!!**
6. Franceschet, Massimo. "XPathMark: an XPath benchmark for the XMark generated data." International XML Database Symposium. Springer, Berlin, Heidelberg, 2005.
7. Afanasiev, Loredana, Massimo Franceschet, and Maarten Marx. "XCheck: a platform for benchmarking XQuery engines." Proceedings of the 32nd international conference on Very large data bases. VLDB Endowment, 2006.
8. Nicola, Matthias, Irina Kogan, and Berni Schiefer. "An XML transaction processing benchmark." Proceedings of the 2007 ACM SIGMOD international conference on Management of data. ACM, 2007.
9. Runapongsa, Kanda, et al. "The Michigan benchmark: towards XML query performance diagnostics." Information Systems 31.2 (2006): 73-97.
10. Schmidt, Albrecht, et al. "A look back on the XML Benchmark project." Intelligent Search on XML Data. Springer, Berlin, Heidelberg, 2003. 263-278.


## Change Analysis -- Sampling / Triangulation


json	proxy-3.0.jar		proxy-5.0.jar		-PULL,-PATHQUERY   
-- proxy-3.0 was superjar, contained jackson classes (com/fasterxml/jackson/jaxrs/json) 

json	fitnesse-20111025.jar	fitnesse-20190716.jar	-DOM (-)
-- removed json.org, called org/json/Test	main  

json	http4k-format-gson-1.0.0.jar	http4k-format-gson-3.0.0.jar	+DATABIND
-- confirmed, this is written in Kotlin !!! CHANGE TITLE ?? 

json	jackson-3.0.0.jar	jackson-8.0.0.jar	+PULL (+)
-- confirmed, this is the parser lib itself






		
		








package nz.ac.vuw.cs.parserstudy.fastjson;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.io.File;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.HashSet;
import com.google.common.io.Files;

/**
 * fastjson DOM parsing example.
 * @author jens dietrich
 */
public class Example {

    public static void main(String[] args) throws Exception {
        File xmlFile = new File("src/main/resources/email.json");
        String json = Files.toString(xmlFile, Charset.defaultCharset());
        JSONObject root = JSON.parseObject(json);

        Collection<String> recipients = new HashSet<>();
        JSONArray arr = root.getJSONArray("to");
        for (int i=0;i<arr.size();i++) {
            JSONObject to = arr.getJSONObject(i);
            String name = to.getString("display_name");
            recipients.add(name);
        }

        assert recipients.contains("Tim");
        assert recipients.contains("Tom");
        assert 2 == recipients.size();

        System.out.println("email document successfully parsed");
    }

}

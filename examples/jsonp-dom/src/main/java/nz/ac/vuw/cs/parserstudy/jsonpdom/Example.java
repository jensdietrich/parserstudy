package nz.ac.vuw.cs.parserstudy.jsonpdom;


import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * DOM parsing example.
 * @author jens dietrich
 */
public class Example {

    public static void main(String[] args) throws Exception {
        File jsonFile = new File("src/main/resources/email.json");
        InputStream in = new FileInputStream(jsonFile);

        List<String> recipients = new ArrayList<>();
        JsonReader reader = Json.createReader(in);
        JsonObject object = reader.readObject();

        JsonArray arr = object.getJsonArray("to");
        for (int i=0;i<arr.size();i++) {
            JsonObject to = arr.getJsonObject(i);
            String name = to.getString("display_name");
            recipients.add(name);
        }

        assert recipients.contains("Tim");
        assert recipients.contains("Tom");
        assert 2 == recipients.size();

        System.out.println("email document successfully parsed");
    }

}

package nz.ac.vuw.cs.parserstudy.jsonpstax;


import javax.json.Json;
import javax.json.stream.JsonParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * STAX parsing example.
 * @author jens dietrich
 */
public class Example {

    public static void main(String[] args) throws Exception {
        File jsonFile = new File("src/main/resources/email.json");
        InputStream in = new FileInputStream(jsonFile);

        List<String> recipients = new ArrayList<>();
        JsonParser parser = Json.createParser(in);
        boolean toMode = false;
        boolean displayNameMode = false;

        while (parser.hasNext()) {
            JsonParser.Event e = parser.next();
            if (e == JsonParser.Event.KEY_NAME) {
                String elementName = parser.getString();
                if ("to".equals(elementName)) toMode = true;
                if (toMode && "display_name".equals(elementName)) displayNameMode = true;
            }
            if (e == JsonParser.Event.END_ARRAY) {
                toMode = false; // the value of to is an array, so each array end means to is ending
            }
            if (e == JsonParser.Event.END_OBJECT) {
                displayNameMode = false; // the value of display_name is an object, so each object end means displayName is ending
            }
            if (e == JsonParser.Event.VALUE_STRING && displayNameMode) {
                String name = parser.getString();
                recipients.add(name);
            }

        }

        assert recipients.contains("Tim");
        assert recipients.contains("Tom");
        assert 2 == recipients.size();

        System.out.println("email document successfully parsed");
    }

}

package nz.ac.vuw.cs.parserstudy.dom4j;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * DOM4J parsing example.
 * @author jens dietrich
 */
public class Example {

    public static void main(String[] args) throws Exception {
        File xmlFile = new File("src/main/resources/email.xml");
        SAXReader builder = new SAXReader();
        Document doc = builder.read(xmlFile);

        Element root = doc.getRootElement();
        assert root.getName().equals("email");

        List<String> recipients = root.elements("to").stream()
            .flatMap(to -> to.elements("display_name").stream())
            .map(e -> e.getText())
            .collect(Collectors.toList());

        assert recipients.contains("Tim");
        assert recipients.contains("Tom");
        assert 2 == recipients.size();

        System.out.println("email document successfully parsed");
    }

}

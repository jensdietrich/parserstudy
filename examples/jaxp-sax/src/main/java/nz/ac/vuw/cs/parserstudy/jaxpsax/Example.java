package nz.ac.vuw.cs.parserstudy.jaxpsax;

import org.xml.sax.InputSource;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.List;

/**
 * SAX parsing example.
 * @author jens dietrich
 */
public class Example {

    public static void main(String[] args) throws Exception {

        InputSource input = new InputSource("src/main/resources/email.xml");

        ExtractRecipients handler = new ExtractRecipients();
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        saxParser.parse(input, handler);
        List<String> recipients = handler.getRecipients();

        assert recipients.contains("Tim");
        assert recipients.contains("Tom");
        assert 2 == recipients.size();

        System.out.println("email document successfully parsed");
    }

}

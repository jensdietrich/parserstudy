package nz.ac.vuw.cs.parserstudy.jaxpdom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * DOM parsing example.
 * @author jens dietrich
 */
public class Example {

    public static void main(String[] args) throws Exception {
        File xmlFile = new File("src/main/resources/email.xml");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(xmlFile);

        List<String> recipients = new ArrayList<>();
        Element root = (Element)doc.getElementsByTagName("email").item(0);
        NodeList toNodes = root.getElementsByTagName("to");
        for (int i=0;i<toNodes.getLength();i++) {
            Element toElement = (Element)toNodes.item(i);
            Element displayNameElement = (Element)toElement.getElementsByTagName("display_name").item(0);
            String displayName = displayNameElement.getTextContent();
            recipients.add(displayName);
        }

        assert recipients.contains("Tim");
        assert recipients.contains("Tom");
        assert 2 == recipients.size();

        System.out.println("email document successfully parsed");
    }

}

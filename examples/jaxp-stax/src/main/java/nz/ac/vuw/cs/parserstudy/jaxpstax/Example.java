package nz.ac.vuw.cs.parserstudy.jaxpstax;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * STAX parsing example.
 * @author jens dietrich
 */
public class Example {

    public static void main(String[] args) throws Exception {

        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        InputStream in = new FileInputStream("src/main/resources/email.xml");
        XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

        boolean toMode = false;
        boolean displayNameMode = false;
        List<String> recipients = new ArrayList<>();

        while(eventReader.hasNext()){
            XMLEvent event = eventReader.nextEvent();
            if (event.isStartElement()) {
                String elementName = event.asStartElement().getName().getLocalPart();
                if ("to".equals(elementName)) toMode = true;
                if (toMode && "display_name".equals(elementName)) displayNameMode = true;
            }
            else if (event.isEndElement()) {
                String elementName = event.asEndElement().getName().getLocalPart();
                if ("to".equals(elementName)) toMode = false;
                if (toMode && "display_name".equals(elementName)) displayNameMode = false;
            }
            else if (event.isCharacters() && displayNameMode) {
                recipients.add(event.asCharacters().getData());
            }
        }

        assert recipients.contains("Tim");
        assert recipients.contains("Tom");
        assert 2 == recipients.size();

        System.out.println("email document successfully parsed");
    }

}

package nz.ac.vuw.cs.parserstudy.jdom2;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * JDOM2 parsing example.
 * @author jens dietrich
 */
public class Example {

    public static void main(String[] args) throws Exception {
        File xmlFile = new File("src/main/resources/email.xml");
        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build(xmlFile);

        Element root = doc.getRootElement();
        assert root.getName().equals("email");

        List<String> recipients = root.getChildren("to").stream()
            .flatMap(to -> to.getChildren("display_name").stream())
            .map(e -> e.getText())
            .collect(Collectors.toList());

        assert recipients.contains("Tim");
        assert recipients.contains("Tom");
        assert 2 == recipients.size();

        System.out.println("email document successfully parsed");
    }

}
